﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Device.Location;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using GdzieJaIde.Datamodel;
using GdzieJaIde.Datamodel.Primitive;
using GdzieJaIde.IO;
using GdzieJaIde.Core;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Shell;

namespace GdzieJaIde
{
    public partial class Map : PhoneApplicationPage
    {
        private void DrawZones()
        {
            int pos = 0;
            bool visible;
            foreach (MapPolygon poly in polygons)
            {
                visible = zones.ElementAt(pos).Visible;
                if (visible == true && !googlemap.Children.Contains(poly)) googlemap.Children.Add(poly);
                pos++;
            }
        }

        private void OnPolygonTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MapPolygon poly = sender as MapPolygon;
            int i = polygons.IndexOf(poly);
            MessageBox.Show(zones.ElementAt(i).Description, zones.ElementAt(i).Name, MessageBoxButton.OK);
        }

        private void OnZonesListButtonClick(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //objectListData.Items.Clear();

            //foreach (var id in Data.getZonesList())
            //{
            //    if (Data.getZone(id).Visible == true)
            //    {
            //        TextBlock tb = new TextBlock()
            //        {
            //            Text = Data.getZone(id).Name,
            //            Tag = Data.getZone(id).Id,
            //            FontSize = 24.0,
            //            Padding = new Thickness(20, 5, 20, 5)
            //        };
            //        tb.Tap += OnZoneNameTextBlockTap;
            //        objectListData.Items.Add(tb);
            //    }
            //}

            //SetDialogContentData(HEADER_ZONES);

            string destination = String.Format("/ObjectListPage.xaml?Type=Zone&CurrZone={0}", CurrentZoneID());
            NavigationService.Navigate(new Uri(destination, UriKind.Relative));

            e.Handled = true;
        }

        /*private void OnZoneNameTextBlockTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            Zone zone;
            int i = 0, id = (int)tb.Tag;
            do
            {
                zone = zones.ElementAt(i);
            }
            while (++i < zones.Count && id != zone.Id);

            double lon, lat;
            //CalculateCenterPoint(polygons.ElementAt(i - 1).Locations, out lat, out lon);

            //googlemap.Center = new GeoCoordinate(lat, lon);

            disableRect.Visibility = Visibility.Collapsed;
            objectList.Visibility = Visibility.Collapsed;
        }*/

        public void CalculateCenterPoint(int id, out double lat, out double lon)
        {
            int pos = Data.getZonesList().IndexOf(id);
            lon = 0.0;
            lat = 0.0;
            foreach (GeoCoordinate point in polygons.ElementAt(pos).Locations)
            {
                lon += point.Longitude;
                lat += point.Latitude;
            }
            lon /= polygons.ElementAt(pos).Locations.Count;
            lat /= polygons.ElementAt(pos).Locations.Count;
        }

        private bool PointInPolygon(GeoCoordinate p, LocationCollection points)
        {
            if (points.Count < 3)
            {
                return false;
            }
            bool res = false;
            GeoCoordinate p1, p2;

            GeoCoordinate oldPoint = points[points.Count - 1];

            foreach (GeoCoordinate newPoint in points)
            {
                if (newPoint.Latitude > oldPoint.Latitude)
                {
                    p1 = oldPoint;
                    p2 = newPoint;
                }
                else
                {
                    p1 = newPoint;
                    p2 = oldPoint;
                }

                if (newPoint.Longitude < p.Longitude && oldPoint.Longitude >= p.Longitude
                    || oldPoint.Longitude < p.Longitude && newPoint.Longitude >= p.Longitude)
                {
                    if (newPoint.Latitude + (p.Longitude - newPoint.Longitude) /
                      (oldPoint.Longitude - newPoint.Longitude) * (oldPoint.Latitude
                        - newPoint.Latitude) < p.Latitude)
                    {
                        res = !res;
                    }
                }
                oldPoint = newPoint;
            }
            return res;
        }

        private void OnGeoWatcherPositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            pin.Location = new GeoCoordinate(e.Position.Location.Latitude, e.Position.Location.Longitude);
            googlemap.Center = pin.Location;

            //sprawdzanie czy sie wlazlo do zony
            bool inside = false;
            foreach (MapPolygon poly in polygons)
            {
                if (zones[polygons.IndexOf(poly)].Visible && PointInPolygon(pin.Location, poly.Locations))
                {
                    inside = true;
                    if (CurrentZone == null)
                    {
                        CurrentZone = zones[polygons.IndexOf(poly)];
                        System.Threading.ThreadPool.QueueUserWorkItem(o =>
                           {
                               CurrentZone.onEnter.Invoke();
                               Console.WriteLine("onEnter");
                               Deployment.Current.Dispatcher.BeginInvoke(() =>
                                   {
                                       DrawZones();
                                   });
                           });
                        //MessageBox.Show("wszedl");
                    }
                }
            }

            if (!inside && CurrentZone != null)
            {
                if (CurrentZone.onLeave != null)
                {
                    CurrentZone.onLeave.Invoke();
                    
                }
                CurrentZone = null;
                //MessageBox.Show("wyszedl");

                DrawZones();
            }

        }
    }
}
