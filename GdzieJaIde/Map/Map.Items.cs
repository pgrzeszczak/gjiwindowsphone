﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using GdzieJaIde.Datamodel;
using GdzieJaIde.Datamodel.Primitive;
using GdzieJaIde.IO;
using GdzieJaIde.Core;

namespace GdzieJaIde
{
    public partial class Map : PhoneApplicationPage
    {
        private void FillItemsList()
        {
            objectListData.Items.Clear();
            if (CurrentZone != null)
            {
                foreach (int id in Data.getItemsList())
                {
                    if (Data.getItem(id).Visible == true && CurrentZone.Id == Data.getItem(id).Zone && !bag.Contains(id))
                    {
                        TextBlock tb = new TextBlock()
                        {
                            Text = Data.getItem(id).Name,
                            Tag = id,
                            FontSize = 24.0,
                            Padding = new Thickness(20, 5, 20, 5)
                        };
                        tb.Tap += OnItemNameTextBlockTap;
                        objectListData.Items.Add(tb);
                    }
                }
            }
        }

        private void OnItemsListButtonClick(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //FillItemsList();
            //SetDialogContentData(HEADER_ITEMS);

            string destination = String.Format("/ObjectListPage.xaml?Type=Item&CurrZone={0}", CurrentZoneID());
            NavigationService.Navigate(new Uri(destination, UriKind.Relative));

            e.Handled = true;
        }

        private void OnItemNameTextBlockTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int id = (int)(sender as TextBlock).Tag;
            Item item = MainControl.getInstance().getItem(id);

            SetButton(DialogButtonActions.Pick);
            //itemInfo.Background = new SolidColorBrush((Color)Application.Current.Resources["PhoneBackgroundColor"]);

            SetObjectInfoData(item.Name, id, item.Description);
            Data.getItem(id).onFind.Invoke();
            DrawZones();
        }

        private void OnPickItemButtonClick(object sender, RoutedEventArgs e)
        {
            Data.getItem((int)objectName.Tag).onPick.Invoke();
            DrawZones();
            bag.Add((int)objectName.Tag);
            objectListData.Items.RemoveAt(objectListData.SelectedIndex);
            itemInfo.Visibility = Visibility.Collapsed;
        }

        private void FillBagList()
        {
            objectListData.Items.Clear();
            foreach (int id in bag)
            {
                TextBlock tb = new TextBlock()
                {
                    Text = Data.getItem(id).Name,
                    Tag = id,
                    FontSize = 24.0,
                    Padding = new Thickness(20, 5, 20, 5)
                };
                tb.Tap += OnBagItemNameTextBlockTap;
                objectListData.Items.Add(tb);
            }
        }

        private void OnBagListButtonClick(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FillBagList();
            SetDialogContentData(HEADER_BAG);
        }

        private void OnBagItemNameTextBlockTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int id = (int)(sender as TextBlock).Tag;
            Item item = MainControl.getInstance().getItem(id);

            SetButton(DialogButtonActions.Drop);
            //itemInfo.Background = new SolidColorBrush((Color)Application.Current.Resources["PhoneBackgroundColor"]);
            SetObjectInfoData(item.Name, id, item.Description);
        }

        private void OnDropItemButtonClick(object sender, RoutedEventArgs e)
        {
            //Data.getItem((int)objectName.Tag).onDrop.Invoke();
            DrawZones();
            bag.Remove((int)objectName.Tag);
            objectListData.Items.RemoveAt(objectListData.SelectedIndex);
            itemInfo.Visibility = Visibility.Collapsed;
        }
    }
}
