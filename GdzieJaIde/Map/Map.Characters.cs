﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using GdzieJaIde.Datamodel;
using GdzieJaIde.Datamodel.Primitive;
using GdzieJaIde.IO;
using GdzieJaIde.Core;

namespace GdzieJaIde
{
    public partial class Map : PhoneApplicationPage
    {
        private void OnNPCListButtonClick(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //objectListData.Items.Clear();

            //if (CurrentZone != null)
            //{
            //    foreach (var id in MainControl.getInstance().getCharactersList())
            //    {
            //        if (Data.getCharacter(id).Visible == true && CurrentZone.Id == Data.getCharacter(id).Zone)
            //        {
            //            TextBlock tb = new TextBlock()
            //            {
            //                Text = Data.getCharacter(id).Name,
            //                Tag = Data.getCharacter(id).Id,
            //                FontSize = 24.0,
            //                Padding = new Thickness(20, 5, 20, 5)
            //            };
            //            tb.Tap += OnCharacterNameTextBlockTap;
            //            objectListData.Items.Add(tb);
            //        }
            //    }
            //}
            //SetDialogContentData(HEADER_CHARACTERS);
            string destination = String.Format("/ObjectListPage.xaml?Type=Character&CurrZone={0}", CurrentZoneID());
            NavigationService.Navigate(new Uri(destination, UriKind.Relative));

            e.Handled = true;
        }

        private void OnCharacterNameTextBlockTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int id = (int)(sender as TextBlock).Tag;
            Character npc = Data.getCharacter(id);

            SetButton(DialogButtonActions.Talk);
            //itemInfo.Background = new SolidColorBrush((Color)Application.Current.Resources["PhoneBackgroundColor"]);
            SetObjectInfoData(npc.Name, id, npc.Description);
        }
    }
}
