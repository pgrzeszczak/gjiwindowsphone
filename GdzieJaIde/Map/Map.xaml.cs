﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Device.Location;
using GdzieJaIde.Datamodel;
using GdzieJaIde.Datamodel.Primitive;
using GdzieJaIde.IO;
using GdzieJaIde.Core;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;

namespace GdzieJaIde
{
    public enum GoogleTileTypes
    {
        Street
    }

    public enum DialogButtonActions
    {
        Pick, Drop, Talk
    }

    public partial class Map : PhoneApplicationPage
    {
        private const string HEADER_ZONES = "zones";
        private const string HEADER_ITEMS = "items";
        private const string HEADER_CHARACTERS = "characters";
        private const string HEADER_BAG = "bag";
        //private const int DIALOG_PICK = 0;
        //private const int DIALOG_DROP = 1;
        //private const int DIALOG_TALK = 2;

        private GeoCoordinateWatcher geoWatcher;

        GoogleTile gt;
        Pushpin pin = new Pushpin()
        {
            Opacity = 0.75,
            Background = new SolidColorBrush(Colors.Red)
        };

        List<Zone> zones = new List<Zone>();
        List<MapPolygon> polygons = new List<MapPolygon>();
        List<Item> items = new List<Item>();

        List<int> bag = new List<int>();

        Zone CurrentZone { get; set; }

        private MainControl Data
        {
            get { return MainControl.getInstance(); }
        }

        public double WindowHeight
        {
            get { return Application.Current.RootVisual.RenderSize.Height; }
        }
        public double WindowWidth
        {
            get { return Application.Current.RootVisual.RenderSize.Width; }
        }

        public Map()
        {
            InitializeComponent();
            (Application.Current as App).MapPage = this;

            LoadData();

            googlemap.Children.Add(pin);
            gt = new GoogleTile();
            googlemap.ZoomBarVisibility = System.Windows.Visibility.Visible;
            googlemap.ZoomLevel = 15;
            //double lon = 16.9486;
            //double lat = 52.4304;
            //setCenterPointTest(lat, lon);

            geoWatcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
            geoWatcher.PositionChanged += OnGeoWatcherPositionChanged;
            geoWatcher.Start();

            DrawZones();
        }

        private void LoadData()
        {
            Data.removeAll();
            zones.Clear();
            items.Clear();
            polygons.Clear();
            bag.Clear();

            Console.WriteLine("                ");
            foreach (var v in googlemap.Children) Console.WriteLine(v.ToString());
            Console.WriteLine("                ");
            var file = IsolatedStorageFile.GetUserStoreForApplication();
            if (file.FileExists("currentscenario/scenario.xml"))
            {
                Console.WriteLine("ISTNIEJEJJEJEJE O TAK!");
            }
            bool check = Data.loadDataFromScenario("TestScenario.xml");
            Console.WriteLine("CHECK: " + check);
            //Console.WriteLine(xml.getScenario());
            System.Threading.ThreadPool.QueueUserWorkItem(o =>
                {
                    Data.getScenario().onBegin.Invoke();
                });
            PageTitle.Text = Data.getScenario().Name;
/*
            Console.WriteLine(Data.getScenario().ToString());
            Console.WriteLine("AAAAAAAAAAAAAAAAAAAA");
            Console.WriteLine(Data.getZone(1));
            Console.WriteLine(Data.getObject(1));
            Console.WriteLine("BBBBBBBBBBBBBBBBBB");
            Console.WriteLine(Data.getItem(154).ToString());
            Console.WriteLine(Data.getObject(154));
            Console.WriteLine("CCCCCCCCCCCCCCCCCCCCCC");
            */
            foreach (var zoneId in Data.getZonesList())
            {
                Zone zone = Data.getZone(zoneId);
                zones.Add(zone);
                Console.WriteLine(zone);

                MapPolygon polygon = new MapPolygon()
                {
                    StrokeThickness = 5,
                    Opacity = 0.3,
                    Fill = new SolidColorBrush((Color)Application.Current.Resources["PhoneAccentColor"]),
                    Stroke = new SolidColorBrush(Colors.Black),
                    Locations = new LocationCollection()
                };
                //polygon.Visibility = SetVisibility(zone.Visible);

                for (int i = 0; i < zone.Points.Count; i++)
                {
                    polygon.Locations.Add(new GeoCoordinate(zone.Points.ElementAt(i).X, zone.Points.ElementAt(i).Y));
                }
                polygon.Tap += OnPolygonTap;

                polygons.Add(polygon);
            }
            /*
            foreach (var primitive in xml.getPrimitives())
            {
                Data.addObject(primitive.Id, primitive);
            }

            foreach (var media in xml.getMedias())
            {
                Data.addObject(media.Id, media);
            }
            */
            foreach (var itemId in Data.getItemsList())
            {
                Item item = Data.getItem(itemId);
                items.Add(item);
                //Console.WriteLine(item);
            }
            /*
            foreach (var quest in xml.getQuests())
            {
                Data.addObject(quest.Id, quest);
            }

            foreach (var timer in xml.getTimers())
            {
                Data.addObject(timer.Id, timer);
            }
            foreach (var character in xml.getCharacters())
            {
                Data.addObject(character.Id, character);
            }
            */
            //((Item)Data.getObject(1112)).onFind.Invoke();

            //Console.WriteLine("visible: {0}", ((Zone)Data.getObject(1)).Visible);
            //((Zone)Data.getObject(1)).onEnter.Invoke();

            //Console.WriteLine("visible: {0}", ((Zone)Data.getObject(1)).Visible);
            //Console.WriteLine("value: {0}", ((BooleanObject)Data.getObject(104)).Value);
            //Console.WriteLine("value: {0}", ((IntegerObject)Data.getObject(105)).Value);
            //Console.WriteLine("silnia: {0}", ((IntegerObject)Data.getObject(100)).Value);
        }

        private void setCenterPointTest(double lat, double lon)
        {
            googlemap.Center = new GeoCoordinate(lat, lon);
        }

        private Visibility SetVisibility(bool b)
        {
            if (b == true) return Visibility.Visible;
            else return Visibility.Collapsed;
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            var border = (((sender as Button).Parent as Grid).Parent as Border);
            if (border.Name.Equals("objectList") || border.Name.Equals("dialogBox")) disableRect.Visibility = Visibility.Collapsed;
            //zonesChoice.Visibility = Visibility.Collapsed;
            objectListData.SelectedItem = null;
            border.Visibility = Visibility.Collapsed;
        }

        //private void OnHomeButtonClick(object sender, RoutedEventArgs e)
        //{
        //    googlemap.Center = pin.Location;
        //}

        private void SetDialogContentData(string header)
        {
            txtblk.Text = String.Format(header);

            objectListData.Height = WindowHeight * 0.5;
            (objectDesc.Parent as ListBox).MaxHeight = WindowHeight * 0.5;
            objectListData.Width = WindowWidth * 0.7;

            //objectList.Background = new SolidColorBrush((Color)Application.Current.Resources["PhoneBackgroundColor"]);
            //objectList.BorderBrush = new SolidColorBrush((Color)Application.Current.Resources["PhoneBackgroundColor"]);

            disableRect.Visibility = Visibility.Visible;
            objectList.Visibility = Visibility.Visible;
        }

        private void SetButton(DialogButtonActions content)
        {
            switch (content)
            {
                case DialogButtonActions.Drop:
                    dropButton.Visibility = Visibility.Visible;
                    actionButton.Visibility = Visibility.Collapsed;
                    talkButton.Visibility = Visibility.Collapsed;
                    break;
                case DialogButtonActions.Pick:
                    dropButton.Visibility = Visibility.Collapsed;
                    actionButton.Visibility = Visibility.Visible;
                    talkButton.Visibility = Visibility.Collapsed;
                    break;
                case DialogButtonActions.Talk:
                    dropButton.Visibility = Visibility.Collapsed;
                    actionButton.Visibility = Visibility.Collapsed;
                    talkButton.Visibility = Visibility.Visible;
                    break;
            }
        }

        private void SetObjectInfoData(string name, int id, string desc)
        {
            objectName.Text = name;
            objectName.Tag = id;
            objectDesc.Text = desc;

            objectDesc.Width = WindowWidth * 0.8;
            (objectName.Parent as ScrollViewer).Width = WindowWidth * 0.8;
            (objectDesc.Parent as ListBox).Height = WindowHeight * 0.5;

            itemInfo.Visibility = Visibility.Visible;
        }

        private void OnHomeButtonClick(object sender, System.Windows.Input.GestureEventArgs e)
        {
            googlemap.Center = pin.Location;
        }

        private int CurrentZoneID()
        {
            if (CurrentZone != null)
            {
                return CurrentZone.Id;
            }
            else
            {
                return -1;
            }
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            DrawZones();

            base.OnNavigatedTo(e);
        }

        private void OnQuestListButtonClick(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string destination = String.Format("/ObjectListPage.xaml?Type=Quest&CurrZone={0}", CurrentZoneID());
            NavigationService.Navigate(new Uri(destination, UriKind.Relative));

            e.Handled = true;
        }
    }

    public class GoogleTile : Microsoft.Phone.Controls.Maps.TileSource
    {
        public int Server { get; set; }
        public char MapMode { get; set; }

        private GoogleTileTypes tileTypes;
        public GoogleTileTypes TileTypes
        {
            get { return tileTypes; }
            set
            {
                tileTypes = value;
                //MapMode = MapModeConverter(value);
                MapMode = 'm';
            }
        }

        public GoogleTile()
        {
            UriFormat = @"http://mt{0}.google.com/vt/lyrs={1}&z={2}&x={3}&y={4}";
            Server = 0;
        }

        public override Uri GetUri(int x, int y, int zoomLevel)
        {
            if (zoomLevel > 0)
            {
                var Url = string.Format(UriFormat, Server, MapMode, zoomLevel, x, y);
                return new Uri(Url);
            }
            return null;
        }

        /*private char MapModeConverter(GoogleTileTypes tiletype)
        {
            switch (tiletype)
            {
                case GoogleTileTypes.Hybrid:
                    {
                        return 'y';
                    }
                case GoogleTileTypes.Physical:
                    {
                        return 't';
                    }
                case GoogleTileTypes.Satellite:
                    {
                        return 's';
                    }
                case GoogleTileTypes.Street:
                    {
                        return 'm';
                    }
                case GoogleTileTypes.WaterOverlay:
                    {
                        return 'r';
                    }
            }
            return ' ';
        } */

    }
}