﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using GdzieJaIde.Core;
using GdzieJaIde.Datamodel;

namespace GdzieJaIde
{
    public partial class ObjectInfoPage : PhoneApplicationPage
    {
        private ScenarioObject _objectType;
        private int _id;
        private Character _character;
        private Item _item;

        private MainControl Data
        {
            get
            {
                return MainControl.getInstance();
            }
        }

        public string ObjectName
        {
            get;
            private set;
        }

        public string Description
        {
            get;
            private set;
        }

        public BitmapImage IconImage
        {
            get
            {
                BitmapImage bi = new BitmapImage();
                bi.SetSource(Application.GetResourceStream(new Uri(_path, UriKind.Relative)).Stream);
                return bi;
            }
        }

        private string _path = "testPicture.jpg";

        public ObjectInfoPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            IDictionary<string, string> parameters = this.NavigationContext.QueryString;
            if (parameters.ContainsKey("Type"))
            {
                _objectType = (ScenarioObject)Enum.Parse(typeof(ScenarioObject), parameters["Type"], true);
            }
            if (parameters.ContainsKey("Id"))
            {
                _id = Int32.Parse(parameters["Id"]);
            }

            FillPage();

            base.OnNavigatedTo(e);
        }

        private void FillPage()
        {
            if (_objectType == ScenarioObject.Item)
            {
                ActionButton.Content = "pick";
                _path = String.Format("assets/{0}", Data.getMedia(Data.getItem(_id).Icon).Path);
                ObjectName = Data.getItem(_id).Name;
                Description = Data.getItem(_id).Description;
                if (Data.getItem(_id).onFind != null)
                {
                    System.Threading.ThreadPool.QueueUserWorkItem(o =>
                    {
                        Data.getItem(_id).onFind.Invoke();
                    });
                }
            }
            else if (_objectType == ScenarioObject.Character)
            {
                ActionButton.Content = "talk";
                _path = String.Format("assets/{0}", Data.getMedia(Data.getCharacter(_id).Icon).Path);
                ObjectName = Data.getCharacter(_id).Name;
                Description = Data.getCharacter(_id).Description;
                if (Data.getCharacter(_id).onGoodMorning != null)
                {
                    System.Threading.ThreadPool.QueueUserWorkItem(o =>
                    {
                        Data.getCharacter(_id).onGoodMorning.Invoke();
                    });
                }
            }
            IconImage.SetSource(Application.GetResourceStream(new Uri(_path, UriKind.Relative)).Stream);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //(((App)Application.Current).RootFrame.Content as PhoneApplicationPage).NavigationService.Navigate(new Uri("/ObjectListPage.xaml?Type=Item&CurrZone=1", UriKind.Relative));
        }
    }
}