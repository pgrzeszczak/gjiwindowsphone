﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GdzieJaIde.Datamodel;
using GdzieJaIde.Datamodel.Primitive;
using GdzieJaIde.IO;

namespace GdzieJaIde.Core
{
    public class MainControl
    {
        private static MainControl CONTROL;

        Dictionary<int, object> allObjects;
        private List<int> zones;
        private List<int> characters;
        private List<int> items;
        private List<int> medias;
        private List<int> quests;
        private List<int> timers;
        private List<int> primitives;
        private Scenario scenario;

        public MainControl()
        {
            allObjects=new Dictionary<int, object>();
            zones=new List<int>();
            characters = new List<int>();
            items = new List<int>();
            medias = new List<int>();
            quests = new List<int>();
            timers = new List<int>();
            primitives = new List<int>();

        }

        public bool loadDataFromScenario(string scenarioName)
        {
            try
            {
                ScenarioFromXML xml = new ScenarioFromXML(scenarioName);

                scenario = xml.getScenario();

                foreach (var primitive in xml.getPrimitives())
                {
                    addObject(primitive.Id, primitive);
                    addPrimitives(primitive.Id);
                }

                Console.WriteLine("prymitywy ok");

                foreach (var media in xml.getMedias())
                {
                    addObject(media.Id, media);
                    addMedia(media.Id);
                }

                Console.WriteLine("media ok");

                foreach (var item in xml.getItems())
                {
                    addObject(item.Id, item);
                    addItem(item.Id);
                }

                foreach (var quest in xml.getQuests())
                {
                    addObject(quest.Id, quest);
                    addQuest(quest.Id);
                }

                foreach (var timer in xml.getTimers())
                {
                    addObject(timer.Id, timer);
                    addTimer(timer.Id);
                }
                foreach (var character in xml.getCharacters())
                {
                    addObject(character.Id, character);
                    addCharacter(character.Id);
                }
                foreach (var zone in xml.getZones())
                {
                    addObject(zone.Id, zone);
                    addZone(zone.Id);
                }

            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
        public Scenario getScenario()
        {
            return scenario;
        }
        public void addObject(int id, object obj)
        {
            allObjects.Add(id, obj);
        }

        public object getObject(int  id)
        {
            object val=new object();
            allObjects.TryGetValue(id,out val);
            return val;
        }

        public void removeAll()
        {
            allObjects.Clear();
            zones.Clear();
            characters.Clear();
            items.Clear();
            medias.Clear();
            quests.Clear();
            timers.Clear();
            primitives.Clear();
        }

        public static MainControl getInstance()
        {
            if(CONTROL==null)
            {
                CONTROL=new MainControl();
            }
            return CONTROL;
        }

        public Zone getZone(int id)
        {
            return (Zone)getObject(id);
        }

        public int getZonesCount()
        {
            return zones.Count;
        }

        public List<int> getZonesList()
        {
            return zones;
        }

        public Item getItem(int id)
        {
            return (Item)getObject(id);
        }

        public int getItemsCount()
        {
            return items.Count;
        }

        public List<int> getItemsList()
        {
            return items;
        }
        public Media getMedia(int id)
        {
            return (Media)getObject(id);
        }

        public int getMediasCount()
        {
            return medias.Count;
        }

        public List<int> getMediasList()
        {
            return medias;
        }
        public Character getCharacter(int id)
        {
            return (Character)getObject(id);
        }

        public int getCharactersCount()
        {
            return characters.Count;
        }

        public List<int> getCharactersList()
        {
            return characters;
        }
        public Quest getQuest(int id)
        {
            return (Quest)getObject(id);
        }

        public int getQuestsCount()
        {
            return quests.Count;
        }

        public List<int> getQuestList()
        {
            return quests;
        }
        public Timer getTimer(int id)
        {
            return (Timer)getObject(id);
        }

        public int getTimersCount()
        {
            return timers.Count;
        }

        public List<int> getTimersList()
        {
            return timers;
        }
        public PrimitiveObject getPrimitive(int id)
        {
            return (PrimitiveObject)getObject(id);
        }

        public int getPrimitivesCount()
        {
            return primitives.Count;
        }

        public List<int> getPrimitivesList()
        {
            return primitives;
        }

        private void addZone(int i)
        {
            zones.Add(i);
        }
        private void addItem(int i)
        {
            items.Add(i);
        }
        private void addCharacter(int i)
        {
            characters.Add(i);
        }
        private void addMedia(int i)
        {
            medias.Add(i);
        }

        private void addQuest(int i)
        {
            quests.Add(i);
        }
        private void addTimer(int i)
        {
            timers.Add(i);
        }
        private void addPrimitives(int i)
        {
            primitives.Add(i);
        }
        
    }
}
