﻿using System;
using System.Windows;
using GdzieJaIde.API;
using GdzieJaIde.Core;
using GdzieJaIde.Datamodel;
using GdzieJaIde.Datamodel.Primitive;
using GdzieJaIde.IO;
using Microsoft.Phone.Controls;

namespace GdzieJaIde
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            /*
            ScenarioFromXML xml = new ScenarioFromXML("TestScenario.xml");
            Console.WriteLine(xml.getScenario());
            try
            {
                foreach (PrimitiveObject obj in xml.getPrimitives())
                {
                    MainControl.getInstance().addObject(obj.Id, obj);
                  //  Console.WriteLine("primitive: {0}", obj.idname);
                }
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.Message);
            }
          //  Console.WriteLine("value: {0}", ((BooleanObject)MainControl.getInstance().getObject(104)).Value);

            foreach (var zone in xml.getZones())
            {
                MainControl.getInstance().addObject(zone.Id, zone);
                Console.WriteLine(zone);
            } 
            
            foreach (var media in xml.getMedias())
            {
                MainControl.getInstance().addObject(media.Id, media);
               // Console.WriteLine(media);
            }

           // Console.WriteLine("visible: {0}", ((Zone)MainControl.getInstance().getObject(1)).Visible);
            ((Zone)MainControl.getInstance().getObject(1)).onEnter.Invoke();

         //   Console.WriteLine("visible: {0}", ((Zone)MainControl.getInstance().getObject(1)).Visible);
         //   Console.WriteLine("value: {0}", ((BooleanObject)MainControl.getInstance().getObject(104)).Value);
         //   Console.WriteLine("value: {0}", ((IntegerObject)MainControl.getInstance().getObject(105)).Value);
         //   Console.WriteLine("silnia: {0}", ((IntegerObject)MainControl.getInstance().getObject(100)).Value);

            */
        }

        private void OnMapButtonClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Map/Map.xaml", UriKind.Relative));
        }

        private void OnLoginButtonClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void OnListButtonClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Scenarios.xaml", UriKind.Relative));
        }
    }
}