﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using GdzieJaIde.IO;
using GdzieJaIde.Datamodel;

namespace GdzieJaIde
{
    public partial class Scenarios : PhoneApplicationPage
    {
        private ServerClient client;
        private ServerClient client2;

        public Scenarios()
        {
            InitializeComponent();

            client = new ServerClient();
            client.addDownloadListAction(AfterDownload);
            client.downloadPublicScenarioList(0, 10);

            client2 = new ServerClient();
            //client2.addDownloadListAction(AfterPrivateDownload);
            client2.addDLoggingAction(AfterLogin);
            client2.logIn("test", "test");
            //client2.downloadPrivateScenarioList(0, 10);
        }

        private void AfterDownload(Dictionary<int, ScenarioEntity> list)
        {
            Console.WriteLine("AfterDownload");
            foreach (var s in list)
            {
                Console.WriteLine("{0} {1}", s.Key, s.Value.Name);
            }
            ScenarioListBox.ItemsSource = list;
        }

        private void AfterPrivateDownload(Dictionary<int, ScenarioEntity> list)
        {
            Console.WriteLine("AfterPrivateDownload");
            foreach (var s in list)
            {
                Console.WriteLine("{0} {1}", s.Key, s.Value.Name);
            }
            ScenarioListBox2.ItemsSource = list;
        }

        private void AfterLogin(bool result)
        {
            Console.WriteLine("Zalogowano {0}", result);
            if (result == true)
            {
                client2.addDownloadListAction(AfterPrivateDownload);
                client2.downloadPrivateScenarioList(0, 10);
            }
        }

        private void Grid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int id = ((KeyValuePair<int, ScenarioEntity>)ScenarioListBox.SelectedItem).Key;
            Console.WriteLine(id);
            client = new ServerClient();
            client.addDownloadScenarioAction(AfterScenarioDownload);
            client.downloadPublicScenario(id);
        }

        private void AfterScenarioDownload(ScenarioEntity scenario)
        {
            ScenarioFromZip zip = new ScenarioFromZip(scenario.ScenarioPath);
            string path = zip.openScenario();

            
        }
    }
}