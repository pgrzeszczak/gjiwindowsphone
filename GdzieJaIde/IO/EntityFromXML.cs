﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using GdzieJaIde.Datamodel;

namespace GdzieJaIde.IO
{
    public class EntityFromXML
    {
        private XElement root;
        public EntityFromXML(string source)
        {
            root=XDocument.Parse(source).Root;
        }
        public List<ScenarioEntity> getScenarios()
        {
            
            List<ScenarioEntity> scenarios=new List<ScenarioEntity>();
            foreach (var scenarioElement in root.Elements(XName.Get("scenario")))
            {
                ScenarioEntity scenario = new ScenarioEntity();
                scenario.Id = (int)scenarioElement.Attribute(XName.Get("id"));
                scenario.Author = (string)scenarioElement.Attribute(XName.Get("author"));
                scenario.Created = (string)scenarioElement.Attribute(XName.Get("created"));
                scenario.Description = (string)scenarioElement.Attribute(XName.Get("description"));
                scenario.Name = (string)scenarioElement.Attribute(XName.Get("name"));
                scenario.SplashPath = "";
                scenarios.Add(scenario);
            }
            return scenarios;
        }
    }
}
