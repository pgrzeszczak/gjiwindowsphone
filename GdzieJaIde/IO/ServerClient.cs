﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
using GdzieJaIde.Datamodel;

namespace GdzieJaIde.IO
{
    public delegate void DownloadListComplete(Dictionary<int, ScenarioEntity> list);
    public delegate void LoggingResult(bool authorized);
    public delegate void DownloadScenarioComplete(ScenarioEntity scenario);
    public class ServerClient
    {
        public static string SPLASH_DIRECTORY = "splash";
        public static string SCENARIO_DIRECTORY = "scenario";
        private string username;
        private string password;
        public object obj;
        public byte[] image;
        private DownloadListComplete _downloadListComplete;
        private DownloadScenarioComplete _downloadScenarioComplete;
        private LoggingResult loggingResult;
        private Dictionary<int, ScenarioEntity> scenarioList;
        private bool authorized = false;
        private int splashesDownloaded ;
        private int splashesRequested;
        public ServerClient()
        {
            scenarioList=new Dictionary<int, ScenarioEntity>();
            splashesRequested = 0;
            splashesDownloaded = 0;
        }
        public void logIn(string username, string password)
        {
            this.username = username;
            this.password = password;
            WebClient client = new WebClient();
            string url = "http://iris.cs.put.poznan.pl/mobile/userscenarios/0/0";
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(username + ":" + password));
            client.Headers["Authorization"] = "Basic " + encoded;
            client.OpenReadCompleted += new OpenReadCompletedEventHandler(client_OpenReadLogIn);
            client.OpenReadAsync(new Uri(url));
        }

        public void downloadPrivateScenarioList(int index, int limit)
        {
            if (!(splashesRequested == 0 && splashesDownloaded == 0))
            {
                Console.WriteLine("jestem tu");
                return;
            }
            if (authorized)
            {
                WebClient client = new WebClient();
                string url = "http://iris.cs.put.poznan.pl/mobile/userscenarios/" + index + "/" + limit;
                String encoded =
                    System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(username + ":" + password));
                client.Headers["Authorization"] = "Basic " + encoded;
                client.OpenReadCompleted += new OpenReadCompletedEventHandler(client_OpenReadScenarioList);
                client.OpenReadAsync(new Uri(url));
                Console.WriteLine("a ja jestem tu");
            }
            else
            {
                Console.WriteLine("a ja za to jestem tu");
                authorized = false;
                loggingResult.Invoke(false);
            }
        }

        public void downloadPublicScenarioList(int index, int limit)
        {
            if (!(splashesRequested == 0 && splashesDownloaded == 0))
            {
                return;
            }
            WebClient client = new WebClient();
            string url = "http://iris.cs.put.poznan.pl/mobile/publiclist/" + index + "/" + limit;
            client.OpenReadCompleted += new OpenReadCompletedEventHandler(client_OpenReadScenarioList);
            client.OpenReadAsync(new Uri(url));

        }
        private void client_OpenReadLogIn(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                authorized = false;
                loggingResult.Invoke(false);
            }
            else
            {
                authorized = true;
                loggingResult.Invoke(true);
                
            }
           
        }
        private void client_OpenReadScenarioList(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                authorized = false;
                loggingResult.Invoke(false);
                return;
            }
            using (var stream = e.Result)
            using (var reader = new StreamReader(stream))
            {
                string contents = reader.ReadToEnd();
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    EntityFromXML xmlReader=new EntityFromXML(contents);
                    foreach (var scenario in xmlReader.getScenarios())
                    {
                        scenarioList.Add(scenario.Id,scenario);
                        if (authorized)
                        {
                            Console.WriteLine("ściągam prywata");
                            downloadPrivateSplash(scenario.Id);
                        }
                        else
                        {
                            Console.WriteLine("ściągam publika");
                            downloadPublicSplash(scenario.Id);
                        }
                        splashesRequested++;
                    }
                });
            }
        }

        public void downloadPublicSplash(int splashID)
        {
            WebClient client = new WebClient();
            client.BaseAddress = "http://iris.cs.put.poznan.pl/mobile/getsplash/" +
                                                                splashID;
            client.OpenReadCompleted += new OpenReadCompletedEventHandler(client_OpenReadCompletedSplash);
            client.OpenReadAsync(new Uri("http://iris.cs.put.poznan.pl/mobile/getsplash/" + splashID));
        }
        public void downloadPrivateSplash(int splashID)
        {
            WebClient client = new WebClient();
            client.BaseAddress = "http://iris.cs.put.poznan.pl/mobile/getsplash/" +
                                                                splashID;
            client.OpenReadCompleted += new OpenReadCompletedEventHandler(client_OpenReadCompletedSplash);

            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(username + ":" + password));
            client.Headers["Authorization"] = "Basic " + encoded;
            client.OpenReadAsync(new Uri("http://iris.cs.put.poznan.pl/mobile/getsplash/" + splashID));
        }

        void client_OpenReadCompletedSplash(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                string contentDisposition = ((WebClient) sender).ResponseHeaders["content-disposition"];
                string lookFor = "filename=";
                string fileName = "";
                int index = contentDisposition.IndexOf(lookFor, StringComparison.CurrentCultureIgnoreCase);
                if (index >= 0)
                {
                    fileName = contentDisposition.Substring(index + lookFor.Length).Replace("\"", "");
                    // int i1 = fileName.IndexOf("splash", StringComparison.CurrentCultureIgnoreCase);
                    int i2 = fileName.IndexOf(".", StringComparison.CurrentCultureIgnoreCase);
                    int id = Convert.ToInt32(fileName.Substring(6, i2 - 6));
                    fileName = SPLASH_DIRECTORY + "/" + fileName;
                    scenarioList[id].SplashPath = fileName;
                }

                Console.WriteLine("jestemtutja {0}", fileName);
                var file = IsolatedStorageFile.GetUserStoreForApplication();
                if (!file.DirectoryExists(SPLASH_DIRECTORY))
                {
                    file.CreateDirectory(SPLASH_DIRECTORY);
                }
                using (
                    IsolatedStorageFileStream stream = new IsolatedStorageFileStream(fileName, System.IO.FileMode.Create,
                                                                                     file))
                {
                    byte[] buffer = new byte[1024];
                    while (e.Result.Read(buffer, 0, buffer.Length) > 0)
                    {
                        stream.Write(buffer, 0, buffer.Length);
                    }
                    stream.Dispose();
                    file.Dispose();
                }
                if (++splashesDownloaded == splashesRequested)
                {
                    _downloadListComplete.Invoke(scenarioList);
                    splashesDownloaded = 0;
                    splashesRequested = 0;
                }

            }
        }

        public void downloadPublicScenario(int scenarioID)
        {
            WebClient client = new WebClient();
            client.OpenReadCompleted += new OpenReadCompletedEventHandler(client_OpenReadCompletedScenario);
            client.OpenReadAsync(new Uri("http://iris.cs.put.poznan.pl/mobile/getscenario/" + scenarioID));
        }
        public void downloadPrivateScenario(int scenarioID)
        {
            WebClient client = new WebClient();
            client.OpenReadCompleted += new OpenReadCompletedEventHandler(client_OpenReadCompletedScenario);

            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(username + ":" + password));
            client.Headers["Authorization"] = "Basic " + encoded;
            client.OpenReadAsync(new Uri("http://iris.cs.put.poznan.pl/mobile/getscenario/" + scenarioID));
        }

        void client_OpenReadCompletedScenario(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                string contentDisposition = ((WebClient) sender).ResponseHeaders["content-disposition"];
                string lookFor = "filename=";
                string fileName = "";
                int id = -1;
                int index = contentDisposition.IndexOf(lookFor, StringComparison.CurrentCultureIgnoreCase);
                if (index >= 0)
                {
                    fileName = contentDisposition.Substring(index + lookFor.Length).Replace("\"", "");
                    // int i1 = fileName.IndexOf("splash", StringComparison.CurrentCultureIgnoreCase);
                    int i2 = fileName.IndexOf(".", StringComparison.CurrentCultureIgnoreCase);
                    id = Convert.ToInt32(fileName.Substring("scenario".Length, i2 - "scenario".Length));
                    fileName = SCENARIO_DIRECTORY + "/" + fileName;
                    if (scenarioList.ContainsKey(id))
                    {
                        scenarioList[id].ScenarioPath = fileName;
                    }
                    else
                    {
                        ScenarioEntity scenario = new ScenarioEntity();
                        scenario.ScenarioPath = fileName;
                        scenarioList.Add(id, scenario);
                    }
                }

                //  Console.WriteLine("jestemtutja {0}", fileName);
                var file = IsolatedStorageFile.GetUserStoreForApplication();
                if (!file.DirectoryExists(SCENARIO_DIRECTORY))
                {
                    file.CreateDirectory(SCENARIO_DIRECTORY);
                }
                using (
                    IsolatedStorageFileStream stream = new IsolatedStorageFileStream(fileName, System.IO.FileMode.Create,
                                                                                     file))
                {
                    byte[] buffer = new byte[1024];
                    while (e.Result.Read(buffer, 0, buffer.Length) > 0)
                    {
                        stream.Write(buffer, 0, buffer.Length);
                    }
                    stream.Dispose();
                }
                if (id >= 0)
                {
                    _downloadScenarioComplete.Invoke(scenarioList[id]);
                }
                file.Dispose();
            }
            else
            {
                throw new FileNotFoundException();
            }
        }

        public void addDownloadListAction(DownloadListComplete action)
        {
            _downloadListComplete = action;
        }
        public void addDLoggingAction(LoggingResult action)
        {
            loggingResult+= action;
        }
        public void addDownloadScenarioAction(DownloadScenarioComplete action)
        {
            _downloadScenarioComplete = action;
        }
    }
}
