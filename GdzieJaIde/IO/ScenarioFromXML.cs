﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Xml.Linq;
using GdzieJaIde.API;
using GdzieJaIde.Actions;
using GdzieJaIde.Datamodel;
using GdzieJaIde.Datamodel.Condition;
using GdzieJaIde.Datamodel.Enums;
using GdzieJaIde.Datamodel.Exceptions;
using GdzieJaIde.Datamodel.Primitive;

namespace GdzieJaIde.IO
{
    public class ScenarioFromXML
    {
        private const string NODE_SCENARIO = "scenario";
        private const string NODE_OBJECTS = "objects";
        private const string NODE_ZONES = "zones";
        private const string NODE_ZONE = "zone";
        private const string NODE_MEDIA = "media";
        private const string NODE_QUEST = "quest";
        private const string NODE_CHARACTER = "character";
        private const string NODE_ITEM = "item";
        private const string NODE_TIMER = "timer";
        private const string NODE_POINTS = "points";
        private const string NODE_POINT = "point";
        private const string NODE_EVENTS = "events";
        private const string NODE_EVENT = "event";
        private const string NODE_CALL_FUNCTION = "callFunction";
        private const string NODE_ARGUMENTS = "arguments";
        private const string NODE_ARGUMENT = "argument";
        private const string NODE_IF_STATEMENT = "ifStatement";
        private const string NODE_CONDITIONS = "conditions";
        private const string NODE_CONDITION = "condition";
        private const string NODE_AND = "and";
        private const string NODE_OR = "or";
        private const string NODE_IF = "if";
        private const string NODE_ELSE = "else";
        private const string NODE_FOR = "for";
        private const string NODE_WHILE = "while";
        private const string NODE_DO = "do";
        private const string NODE_PRIMITIVES = "primitives";
        private const string NODE_INTEGER = "integer";
        private const string NODE_BOOLEAN = "boolean";
        private const string NODE_DOUBLE = "double";
        private const string NODE_CHAR = "char";
        private const string NODE_STRING = "string";

        private const string ATTRIBUTE_CREATED = "created";
        private const string ATTRIBUTE_NAME = "name";
        private const string ATTRIBUTE_AUTHOR = "author";
        private const string ATTRIBUTE_CREATOR_VERSION = "creatorVersion";
        private const string ATTRIBUTE_DESCRIPTION = "description";
        private const string ATTRIBUTE_VERSION = "version";
        private const string ATTRIBUTE_ID = "id";
        private const string ATTRIBUTE_IDNAME = "idname";
        private const string ATTRIBUTE_RADIUS = "radius";
        private const string ATTRIBUTE_STATE = "state";
        private const string ATTRIBUTE_VISIBLE = "visible";
        private const string ATTRIBUTE_SEX = "sex";
        private const string ATTRIBUTE_ZONE = "zone";
        private const string ATTRIBUTE_PATH = "path";
        private const string ATTRIBUTE_TYPE = "type";
        private const string ATTRIBUTE_INTERVAL = "interval";
        private const string ATTRIBUTE_VALUE = "value";
        private const string ATTRIBUTE_LATITUDE = "latitude";
        private const string ATTRIBUTE_LONGITIUDE = "longitude";
        private const string ATTRIBUTE_TARGET = "target";
        private const string ATTRIBUTE_EAST_SIDE = "eastSide";
        private const string ATTRIBUTE_EAST_SIDE_TYPE = "eastSideType";
        private const string ATTRIBUTE_OPERATOR = "operator";
        private const string ATTRIBUTE_WEST_SIDE = "westSide";
        private const string ATTRIBUTE_WEST_SIDE_TYPE = "westSideType";
        private const string ATTRIBUTE_START_VALUE = "startValue";
        private const string ATTRIBUTE_END_VALUE = "endValue";
        private const string ATTRIBUTE_STEP = "step";
        private const string ATTRIBUTE_ICON = "icon";

        private const int DEFAULT_ICON = 0;
        private const string DEFAULT_STATE = "default";

        private XElement root;
        public ScenarioFromXML(string path)
        {
            XDocument loadedCustomData = XDocument.Load(path);

            if (loadedCustomData.Root != null)
            {
                this.root = new XElement(loadedCustomData.Root);
            }
        }
        
        public ScenarioFromXML(IsolatedStorageFile file,string path)
        {
            IsolatedStorageFileStream stream = new IsolatedStorageFileStream(path, System.IO.FileMode.Open,
                                                                                     file);
            XDocument loadedCustomData = XDocument.Load(stream);

            if (loadedCustomData.Root != null)
            {
                this.root = new XElement(loadedCustomData.Root);
            }
            stream.Dispose();
            file.Dispose();
        }

        public string getAttributeValue(XElement node, string attributeName) 
        {
            var attribute = node.Attribute(attributeName);
            if(attribute!=null)
            {
                if(attribute.Value!=null && attribute.Value!="")
                {
                    return attribute.Value;
                }
            }
            throw new NullAttributeValueException(node.Name.ToString(),attributeName);
        }

        public List<PrimitiveObject> getPrimitives()
        {
            try
            {
                List<PrimitiveObject> list=new List<PrimitiveObject>();
                foreach (XElement node in root.Descendants(XName.Get(NODE_PRIMITIVES)).Elements())
                {
                    PrimitiveObject newObject = null;
                    if (node.Name == NODE_BOOLEAN)
                    {
                        newObject = new BooleanObject();
                        ((BooleanObject)newObject).Value = Convert.ToBoolean(getAttributeValue(node,ATTRIBUTE_VALUE));
                    }
                    else if (node.Name == NODE_CHAR)
                    {
                        newObject = new CharObject();
                        ((CharObject)newObject).Value = Convert.ToChar(getAttributeValue(node,ATTRIBUTE_VALUE));

                    }
                    else if (node.Name == NODE_DOUBLE)
                    {
                        newObject = new DoubleObject();
                        ((DoubleObject)newObject).Value = Convert.ToDouble(getAttributeValue(node, ATTRIBUTE_VALUE));
                    }
                    else if (node.Name == NODE_INTEGER)
                    {
                        newObject = new IntegerObject();
                        ((IntegerObject)newObject).Value = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_VALUE));
                    }
                    else if (node.Name == NODE_STRING)
                    {
                        newObject = new StringObject();
                        ((StringObject) newObject).Value = getAttributeValue(node, ATTRIBUTE_VALUE);
                    }

                    else
                    {
                        throw new NodeStructureException(node.Name.ToString(), root.Name.ToString());
                    }
                    newObject.Id = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ID));

                    newObject.Idname = getAttributeValue(node, ATTRIBUTE_IDNAME);
                    if (node.Element(NODE_EVENTS) != null)
                    {
                            
                        foreach (XElement eventElement in node.Element(NODE_EVENTS).Descendants(NODE_EVENT))
                        {
                            String type = getAttributeValue(eventElement, ATTRIBUTE_TYPE);
                            if (type == PrimitiveObject.ON_VALUE_CHANGE)
                            {
                                newObject.onValueChange = getEvent(eventElement);
                            }
                        }

                    }
                    else 
                    {
                        Console.WriteLine("[WARNING] Primitive id= " + newObject.Id + " doesn't contain events");
                    }
                
                    list.Add(newObject);
                }
                return list;
            }
            catch(NullReferenceException ex)
            {
                Console.WriteLine("[WARNING] Scenario doesn't contain primitives");
            }
            return null;
        }

        public Scenario getScenario()
        {
            Scenario scenario = new Scenario();

            scenario.Author = getAttributeValue(root, ATTRIBUTE_AUTHOR);
            scenario.Created = getAttributeValue(root, ATTRIBUTE_CREATED);
            scenario.CreatorVersion = getAttributeValue(root, ATTRIBUTE_CREATOR_VERSION);
            scenario.Description = getAttributeValue(root, ATTRIBUTE_DESCRIPTION);
            scenario.Name = getAttributeValue(root, ATTRIBUTE_NAME);
            scenario.Version = getAttributeValue(root, ATTRIBUTE_VERSION);
            if (root.Element(NODE_EVENTS) != null)
            {

                foreach (XElement eventElement in root.Element(NODE_EVENTS).Descendants(NODE_EVENT))
                {
                    String type = getAttributeValue(eventElement, ATTRIBUTE_TYPE);
                    if (type == Scenario.ON_BEGIN)
                    {
                        scenario.onBegin = getEvent(eventElement);
                    }
                    else if (type == Scenario.ON_END)
                    {
                        scenario.onEnd = getEvent(eventElement);
                    }
                    else if (type == Scenario.ON_RESTORE)
                    {
                        scenario.onRestore = getEvent(eventElement);
                    }
                    else if (type == Scenario.ON_RESTORE)
                    {
                        scenario.onSave = getEvent(eventElement);
                    }
                }
            }
            else
            {
                Console.WriteLine("[WARNING] Scenario doesn't contain events");
            }

            return scenario;
        }

        public LinkedList<Zone> getZones()
        {
            LinkedList<Zone> zones = new LinkedList<Zone>();
              
                foreach (XElement node in root.Descendants(XName.Get(NODE_ZONE)))
                {
                    Zone zone = new Zone();

                    zone.Id = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ID));
                    zone.Idname = getAttributeValue(node, ATTRIBUTE_IDNAME);
                    zone.Description = getAttributeValue(node, ATTRIBUTE_DESCRIPTION);
                    zone.Name = getAttributeValue(node, ATTRIBUTE_NAME);
                    try
                    {
                        zone.Icon = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ICON));
                    }
                    catch (NullAttributeValueException)
                    {
                        zone.Icon = DEFAULT_ICON;
                    } 
                    
                    try
                    {
                        zone.State = getAttributeValue(node, ATTRIBUTE_STATE);
                    }
                    catch (NullAttributeValueException)
                    {
                        zone.State = DEFAULT_STATE;
                    }

                    List<Point> points=new List<Point>();
                    try
                    {

                        foreach (var pointNode in node.Descendants(XName.Get(NODE_POINT)))
                        {
                            points.Add(new Point(Convert.ToDouble(getAttributeValue(pointNode, ATTRIBUTE_LATITUDE)),
                                Convert.ToDouble(getAttributeValue(pointNode, ATTRIBUTE_LONGITIUDE))));
                        }
                        zone.Points = points;
                    }
                    catch (NullReferenceException)
                    {

                        throw new NodeStructureException(NODE_POINT,node.Name.ToString());
                    }
                    
                    /*  if (attributeValue != null)
                    {
                        zone.Points = Convert.ToInt32(attributeValue.Value);

                    }*/

                   try
                   {
                       zone.Radius = Convert.ToDouble(getAttributeValue(node, ATTRIBUTE_RADIUS));
                   }
                   catch(SystemException ex)
                        {
                        }
                    zone.Visible = Convert.ToBoolean(getAttributeValue(node, ATTRIBUTE_VISIBLE));
                    

                    zones.AddLast(zone);
                    if (node.Element(NODE_EVENTS) != null)
                    {

                        foreach (XElement eventElement in node.Element(NODE_EVENTS).Descendants(NODE_EVENT))
                        {
                            String type = getAttributeValue(eventElement, ATTRIBUTE_TYPE);
                            if (type == Zone.ON_ENTER)
                            {
                                zone.onEnter = getEvent(eventElement);
                            }
                            else if (type == Zone.ON_LEAVE)
                            {
                                zone.onLeave = getEvent(eventElement);
                            }
                            else if (type == Zone.ON_STATE_CHANGE)
                            {
                                zone.onStateChange = getEvent(eventElement);
                            }
                            else if (type == Zone.ON_VISIBLE_CHANGE)
                            {
                                zone.onVisibleChange = getEvent(eventElement);
                            }
                        }
                    }
                    else{
                        Console.WriteLine("[WARNING] Zone id= "+zone.Id+ "doesn't contain events");
                    }
                }
            return zones;
            }
        public LinkedList<Media> getMedias()
        {
            try
            {

                LinkedList<Media> medias = new LinkedList<Media>();

                foreach (XElement node in root.Descendants(XName.Get(NODE_MEDIA)))
                {
                    Media media = new Media();

                    media.Id = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ID));
                    try
                    {
                        media.Icon = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ICON));
                    }
                    catch (NullAttributeValueException)
                    {
                        media.Icon = DEFAULT_ICON;
                    }
                    media.Idname = getAttributeValue(node, ATTRIBUTE_IDNAME);
                    media.Path = getAttributeValue(node, ATTRIBUTE_PATH);
                    media.Type =
                        (MediaType)Enum.Parse(typeof(MediaType), getAttributeValue(node, ATTRIBUTE_TYPE), true);


                    medias.AddLast(media);
                    if (node.Element(NODE_EVENTS) != null)
                    {
                        foreach (XElement eventElement in node.Element(NODE_EVENTS).Descendants(NODE_EVENT))
                        {
                            String type = getAttributeValue(eventElement, ATTRIBUTE_TYPE);
                            if (type == Media.ON_PLAY)
                            {
                                media.onPlay = getEvent(eventElement);
                            }
                            else if (type == Media.ON_SHOW)
                            {
                                media.onShow = getEvent(eventElement);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("[WARNING] Media id= " + media.Id + " doesn't contain events");
                    }


                }
                if (medias.Count == 0)
                {
                    Console.WriteLine("[WARNING] Scenario doesn't contain medias");
                }
                return medias;
            }

            catch (NullReferenceException ex)
            {
                Console.WriteLine("[WARNING] Scenario doesn't contain medias");
            }
            return null;
        }

        public LinkedList<Item> getItems()
        {
            try
            {

                LinkedList<Item> items = new LinkedList<Item>();

                foreach (XElement node in root.Descendants(XName.Get(NODE_ITEM)))
                {
                    Item item = new Item();

                    item.Id = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ID));
                    try
                    {
                        item.Icon = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ICON));
                    }
                    catch (NullAttributeValueException)
                    {
                        item.Icon = DEFAULT_ICON;
                    }
                    item.Idname = getAttributeValue(node, ATTRIBUTE_IDNAME);
                    item.Name = getAttributeValue(node, ATTRIBUTE_NAME);
                   /* item.State =
                        (ItemState)Enum.Parse(typeof(ItemState), getAttributeValue(node, ATTRIBUTE_STATE), true);*/
                    item.Visible = Convert.ToBoolean(getAttributeValue(node, ATTRIBUTE_VISIBLE));
                    item.Zone = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ZONE));
                    item.Description = getAttributeValue(node, ATTRIBUTE_DESCRIPTION);
                    try
                    {
                        item.State = getAttributeValue(node, ATTRIBUTE_STATE);
                    }
                    catch (NullAttributeValueException)
                    {
                        item.State = DEFAULT_STATE;
                    }
                    items.AddLast(item);
                    if (node.Element(NODE_EVENTS) != null)
                    {
                        foreach (XElement eventElement in node.Element(NODE_EVENTS).Descendants(NODE_EVENT))
                        {
                            String type = getAttributeValue(eventElement, ATTRIBUTE_TYPE);
                            if (type == Item.ON_DROP)
                            {
                                item.onDrop = getEvent(eventElement);
                            }
                            else if (type == Item.ON_FIND)
                            {
                                item.onFind = getEvent(eventElement);
                            }
                            else if (type == Item.ON_PICK)
                            {
                                item.onPick = getEvent(eventElement);
                            }
                            else if (type == Item.ON_STATE_CHANGE)
                            {
                                item.onStateChange = getEvent(eventElement);
                            }
                            else if (type == Item.ON_VISIBLE_CHANGE)
                            {
                                item.onVisibleChange = getEvent(eventElement);
                            }
                            else if (type == Item.ON_ZONE_CHANGE)
                            {
                                item.onZoneChange = getEvent(eventElement);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("[WARNING] Item id= " + item.Id + " doesn't contain events");
                    }


                }
                if (items.Count==0)
                {
                    Console.WriteLine("[WARNING] Scenario doesn't contain items");
                    
                }
                return items;
            }

            catch (NullReferenceException ex)
            {
                Console.WriteLine("[WARNING] Scenario doesn't contain items");
            }
            return null;
        }

        public LinkedList<Character> getCharacters()
        {
            try
            {

                LinkedList<Character> characters = new LinkedList<Character>();

                foreach (XElement node in root.Descendants(XName.Get(NODE_CHARACTER)))
                {
                    Character character = new Character();

                    character.Id = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ID));
                    try
                    {
                        character.Icon = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ICON));
                    }
                    catch (NullAttributeValueException)
                    {
                        character.Icon = DEFAULT_ICON;
                    }
                    character.Idname = getAttributeValue(node, ATTRIBUTE_IDNAME);
                    character.Name = getAttributeValue(node, ATTRIBUTE_NAME);
                    //character.State =
                    //    (CharacterState)Enum.Parse(typeof(CharacterState), getAttributeValue(node, ATTRIBUTE_STATE), true);
                    character.Visible = Convert.ToBoolean(getAttributeValue(node, ATTRIBUTE_VISIBLE));
                    character.Zone = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ZONE));
                    character.Description = getAttributeValue(node, ATTRIBUTE_DESCRIPTION);
                    character.Sex = Convert.ToChar(getAttributeValue(node, ATTRIBUTE_SEX)[0]);
                    //character.Description = getAttributeValue(node, ATTRIBUTE_DESCRIPTION);
                    try
                    {
                        character.State = getAttributeValue(node, ATTRIBUTE_STATE);
                    }
                    catch (NullAttributeValueException)
                    {
                        character.State = DEFAULT_STATE;
                    }
                    characters.AddLast(character);
                    if (node.Element(NODE_EVENTS) != null)
                    {
                        foreach (XElement eventElement in node.Element(NODE_EVENTS).Descendants(NODE_EVENT))
                        {
                            String type = getAttributeValue(eventElement, ATTRIBUTE_TYPE);
                            if (type == Character.ON_DIALOG)
                            {
                                character.onDialog = getEvent(eventElement);
                            }
                            else if (type == Character.ON_GOOD_BYE)
                            {
                                character.onGoodBye = getEvent(eventElement);
                            }
                            else if (type == Character.ON_GOOD_MORNING)
                            {
                                character.onGoodMorning = getEvent(eventElement);
                            }
                            else if (type == Character.ON_STATE_CHANGE)
                            {
                                character.onStateChange = getEvent(eventElement);
                            }
                            else if (type == Character.ON_VISIBLE_CHANGE)
                            {
                                character.onVisibleChange = getEvent(eventElement);
                            }
                            else if (type == Character.ON_ZONE_CHANGE)
                            {
                                character.onZoneChange = getEvent(eventElement);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("[WARNING] Character id= " + character.Id + " doesn't contain events");
                    }
                }
                if (characters.Count == 0)
                {
                    Console.WriteLine("[WARNING] Scenario doesn't contain characters");
                }
                return characters;
            }

            catch (NullReferenceException ex)
            {
                Console.WriteLine("[WARNING] Scenario doesn't contain characters");
            }
            return null;
        }

        public LinkedList<Quest> getQuests()
        {
            try
            {

                LinkedList<Quest> quests = new LinkedList<Quest>();

                foreach (XElement node in root.Descendants(XName.Get(NODE_QUEST)))
                {
                    Quest quest = new Quest();

                    quest.Id = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ID));
                    try
                    {
                        quest.Icon = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ICON));
                    }
                    catch (NullAttributeValueException)
                    {
                        quest.Icon = DEFAULT_ICON;
                    }
                    quest.Idname = getAttributeValue(node, ATTRIBUTE_IDNAME);
                    quest.Name = getAttributeValue(node, ATTRIBUTE_NAME);
                    quest.Visible = Convert.ToBoolean(getAttributeValue(node, ATTRIBUTE_VISIBLE));
                    quest.Description = getAttributeValue(node, ATTRIBUTE_DESCRIPTION);
                    try
                    {
                        quest.State = getAttributeValue(node, ATTRIBUTE_STATE);
                    }
                    catch (NullAttributeValueException)
                    {
                        quest.State = DEFAULT_STATE;
                    }
                    quests.AddLast(quest);
                    if (node.Element(NODE_EVENTS) != null)
                    {
                        foreach (XElement eventElement in node.Element(NODE_EVENTS).Descendants(NODE_EVENT))
                        {
                            String type = getAttributeValue(eventElement, ATTRIBUTE_TYPE);
                            if (type == Quest.ON_BEGIN)
                            {
                                quest.onBegin = getEvent(eventElement);
                            }
                            else if (type == Quest.ON_COMPLETE)
                            {
                                quest.onComplete = getEvent(eventElement);
                            }
                            else if (type == Quest.ON_INTERACTION)
                            {
                                quest.onInteraction = getEvent(eventElement);
                            }
                            else if (type == Quest.ON_STATE_CHANGE)
                            {
                                quest.onStateChange = getEvent(eventElement);
                            }
                            else if (type == Quest.ON_VISIBLE_CHANGE)
                            {
                                quest.onVisibleChange = getEvent(eventElement);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("[WARNING] Quest id= " + quest.Id + " doesn't contain events");
                    }
                }
                if (quests.Count == 0)
                {
                    Console.WriteLine("[WARNING] Scenario doesn't contain quests");
                }
                return quests;
            }

            catch (NullReferenceException ex)
            {
                Console.WriteLine("[WARNING] Scenario doesn't contain quest");
            }
            return null;
        }

        public LinkedList<Timer> getTimers()
        {
            try
            {

                LinkedList<Timer> timers = new LinkedList<Timer>();

                foreach (XElement node in root.Descendants(XName.Get(NODE_TIMER)))
                {
                    Timer timer = new Timer();

                    timer.Id = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ID));
                    try
                    {
                        timer.Icon = Convert.ToInt32(getAttributeValue(node, ATTRIBUTE_ICON));
                    }
                    catch (NullAttributeValueException)
                    {
                        timer.Icon = DEFAULT_ICON;
                    }
                    timer.Idname = getAttributeValue(node, ATTRIBUTE_IDNAME);
                    timer.Interval = Convert.ToInt64(getAttributeValue(node, ATTRIBUTE_INTERVAL));

                    timers.AddLast(timer);
                    if (node.Element(NODE_EVENTS) != null)
                    {
                        foreach (XElement eventElement in node.Element(NODE_EVENTS).Descendants(NODE_EVENT))
                        {
                            String type = getAttributeValue(eventElement, ATTRIBUTE_TYPE);
                            if (type == Timer.ON_START)
                            {
                                timer.onStart = getEvent(eventElement);
                            }
                            else if (type == Timer.ON_STOP)
                            {
                                timer.onStop = getEvent(eventElement);
                            }
                            else if (type == Timer.ON_TIMER)
                            {
                                timer.onTimer = getEvent(eventElement);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("[WARNING] Timer id= " + timer.Id + " doesn't contain events");
                    }
                }
                if (timers.Count == 0)
                {
                    Console.WriteLine("[WARNING] Scenario doesn't contain timers");
                }
                return timers;
            }

            catch (NullReferenceException ex)
            {
                Console.WriteLine("[WARNING] Scenario doesn't contain timers");
            }
            return null;
        }


        public EventDelegate.Event getEvent(XElement eventElement)
        {
            EventDelegate.Event rootEvent=new EventDelegate.Event(delegate () { });
            foreach (XElement element in eventElement.Elements())
            {
                
                rootEvent += getSingleAction(element).Invoke;
                
               // Console.WriteLine("elo");
              //  Console.WriteLine(element.Name);
               // Console.WriteLine(element.Attribute(ATTRIBUTE_NAME));
                ;
            };
            return rootEvent;
        }

        public EventDelegate.Event getSingleAction(XElement element)
        {
            EventDelegate.Event rootEvent = new EventDelegate.Event(delegate() { });

            if (element.Name==NODE_CALL_FUNCTION)
            {
                rootEvent += getCallFunction(element).Invoke;
            }
            else if (element.Name==NODE_IF_STATEMENT)
            {
                rootEvent += getIfStatement(element).Invoke;
            }
            else if (element.Name==NODE_FOR)
            {
                rootEvent += getFor(element).Invoke;
            }
            else if (element.Name==NODE_WHILE)
            {
                rootEvent += getWhile(element).Invoke;
            }

            return rootEvent;
        }

        private EventDelegate.Event getWhile(XElement element)
        {
            WhileAction action=new WhileAction();

            action.Condition = getConditions(element.Element(NODE_CONDITIONS));
            try
            {
                foreach (XElement child in element.Element(NODE_DO).Elements())
                {
                    action.addActionToBody(getSingleAction(child));

                }
            }
            catch (NullReferenceException)
            {
                throw new NodeStructureException(NODE_DO,element.Name.ToString());
            }
            return action.invoke;
        }

        private EventDelegate.Event getFor(XElement element)
        {

            ForAction action=new ForAction();

            action.Starts = Convert.ToInt32(getAttributeValue(element, ATTRIBUTE_START_VALUE));
            action.Ends = Convert.ToInt32(getAttributeValue(element, ATTRIBUTE_END_VALUE));
            action.Step = Convert.ToInt32(getAttributeValue(element, ATTRIBUTE_STEP));
            try
            {
                foreach (XElement childElement in element.Elements())
                {
                    action.addActionToBody(getSingleAction(childElement));
                }
            }
            catch (NullReferenceException)
            {
                throw new NodeStructureException("For doesn't contain body",element.Name.ToString());
            }

            return action.invoke;
        }

        private EventDelegate.Event getIfStatement(XElement element)
        {
            IfAction action=new IfAction();

            

            action.Condition = getConditions(element.Element(NODE_CONDITIONS));
            try
            {
                foreach (XElement child in element.Element(NODE_IF).Elements())
                {
                    action.addActionToTrueBody(getSingleAction(child));

                }
            }
            
            catch (NullReferenceException)
            {
                throw new NodeStructureException(NODE_IF,element.Name.ToString());
            }

            try
            {

                foreach (XElement child in element.Element(NODE_ELSE).Elements())
                {
                    action.addActionToFalseBody(getSingleAction(child));

                }
            }
            catch (NullReferenceException)
            {
            }

            return action.invoke;
        }

        private EventDelegate.Event getCallFunction(XElement element)
        {
            CallAction action=new CallAction();

            action.FunctionName = getAttributeValue(element, ATTRIBUTE_NAME);
            action.ClassName = (ClassType)Enum.Parse(typeof(ClassType), getAttributeValue(element, ATTRIBUTE_TYPE), true);

            try
            {
                action.Target = new Argument(ObjectType.VALUE, getAttributeValue(element, ATTRIBUTE_TARGET));
            }
            catch (NullAttributeValueException)
            {
            }


            List<Argument> arguments=new List<Argument>();
            try
            {
                foreach (XElement argument in element.Descendants(NODE_ARGUMENT))
                {
                    arguments.Add(getArgument(argument));
                }
            }
            catch(NullReferenceException)
            {
                throw new NodeStructureException(NODE_ARGUMENTS, NODE_CALL_FUNCTION + getAttributeValue(element, ATTRIBUTE_NAME));
            }

            action.Arguments = arguments.ToArray();

            return action.invoke;
        }
        private ComplexCondition getConditions(XElement element)
        {
            ComplexCondition condition=new ComplexCondition();
            condition.Type=ComplexCondition.ConditionType.AND;
            getCondition(element, condition);
            return condition;
        }

        private void getCondition(XElement element, ComplexCondition parentCondition)
        {
            try
            {
                foreach (XElement conditionElement in element.Elements())
                {
                    string name = conditionElement.Name.ToString();
                    if (name == NODE_CONDITION)
                    {
                        Datamodel.Condition.Condition condition = new Datamodel.Condition.Condition();
                        string eastValue = null, westValue = null;
                        Datamodel.Condition.Condition.OperatorType operatorType =
                            Datamodel.Condition.Condition.OperatorType.EQUAL;
                        ObjectType westType = ObjectType.INTEGER, eastType = ObjectType.INTEGER;

                        eastValue = getAttributeValue(conditionElement, ATTRIBUTE_EAST_SIDE);
                        westValue = getAttributeValue(conditionElement, ATTRIBUTE_WEST_SIDE);

                        operatorType =
                            (Datamodel.Condition.Condition.OperatorType)
                            Enum.Parse(typeof (Datamodel.Condition.Condition.OperatorType),
                                       getAttributeValue(conditionElement, ATTRIBUTE_OPERATOR), true);
                        eastType =
                            (ObjectType)
                            Enum.Parse(typeof (ObjectType),
                                       getAttributeValue(conditionElement, ATTRIBUTE_EAST_SIDE_TYPE), true);
                        westType =
                            (ObjectType)
                            Enum.Parse(typeof (ObjectType),
                                       getAttributeValue(conditionElement, ATTRIBUTE_WEST_SIDE_TYPE), true);

                        condition.EastSideType = eastType;
                        condition.WestSideType = westType;
                        condition.EastSideValue = eastValue;
                        condition.WestSideValue =westValue;
                        condition.Op = operatorType;

                        parentCondition.addCondition(condition);
                    }
                    else
                    {
                        ComplexCondition condition = new ComplexCondition();
                        ComplexCondition.ConditionType conditionType;
                        conditionType =
                            (ComplexCondition.ConditionType)
                            Enum.Parse(typeof (ComplexCondition.ConditionType), name, true);
                        condition.Type = conditionType;
                        getCondition(conditionElement, condition);
                        parentCondition.addCondition(condition);
                    }
                }
            }
            catch(NullReferenceException)
            {
                throw new NodeStructureException(element.Name.ToString(),parentCondition.Type.ToString());
            }
        }

        private Argument getArgument(XElement argument)
        {
            int id;
            string value=value = getAttributeValue(argument, ATTRIBUTE_VALUE);
            var type = (ObjectType) Enum.Parse(typeof(ObjectType),getAttributeValue(argument, ATTRIBUTE_TYPE),true);
            
            return new Argument(type,value);
        }

    }
}
