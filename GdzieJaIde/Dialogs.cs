﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Threading;
using System.ComponentModel;
using Microsoft.Phone.Controls;

namespace GdzieJaIde
{
    public class Dialogs
    {
        //public enum DialogStates
        //{
        //    Active, Finished, Free
        //}

        private Map map;
        private ObjectInfoPage objInfoPage;
        private static bool active = false;
        private static string resultTxt = "";
        //public static int result = -1;
        public int result = 0;

        private BackgroundWorker bw = new BackgroundWorker();

        public Dialogs()
        {

        }

        private Button AddButton()
        {
            Button b = new Button()
            {
                Content = "OK"
            };
            b.Click += OnButton1Click;
            Grid.SetRow(b, 2);
            return b;
        }

        //public static int finished = 0;

        private void CheckPageType()
        {
            var currentPage = ((PhoneApplicationFrame)Application.Current.RootVisual).Content;
            if (currentPage.GetType().ToString().EndsWith(".Map"))
            {
                map = currentPage as Map;
            }
            else if (currentPage.GetType().ToString().EndsWith(".ObjectInfoPage"))
            {
                objInfoPage = currentPage as ObjectInfoPage;
            }
            Console.WriteLine(currentPage.GetType());
        }

        public void ShowMessage(string title, string message, int id)
        {
            //if (state == DialogStates.Free && finished == id) state = DialogStates.Active;
            
            //ThreadPool.QueueUserWorkItem(o =>
            //    {
                    //while (finished != id)
                    //{
                    //    Thread.Sleep(100);
                    //}
                    result = 1;

                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            CheckPageType();
                            MyDialog md = new MyDialog(title, message);
                            md.AddMessage();
                            md.dialogButton.Click += delegate(object sender, RoutedEventArgs e)
                            {
                                //md.dialogBox.Visibility = Visibility.Collapsed;
                                result = 0;
                                //state = DialogStates.Free;
                                //finished++;
                                if (map != null)
                                {
                                    map.disableRect.Visibility = Visibility.Collapsed;
                                    map.ContentPanel.Children.Remove(md.dialogBox);
                                }
                                else if (objInfoPage != null)
                                {
                                    objInfoPage.disableRect.Visibility = Visibility.Collapsed;
                                    objInfoPage.ContentPanel.Children.Remove(md.dialogBox);
                                }
                            };

                            if (map != null)
                            {
                                map.disableRect.Visibility = Visibility.Visible;
                                map.ContentPanel.Children.Add(md.dialogBox);
                            }
                            else if (objInfoPage != null)
                            {
                                objInfoPage.disableRect.Visibility = Visibility.Visible;
                                objInfoPage.ContentPanel.Children.Add(md.dialogBox);
                            }
                        });
                   //while (state == DialogStates.Active)
            while (result == 1)
                    {
                        Thread.Sleep(100);
                    }

                //});
        }

        public string ShowDialog(string title, string message, int id)
        {
            //if (state == DialogStates.Free && finished == id) state = DialogStates.Active;
            //ThreadPool.QueueUserWorkItem(o =>
            //{
                //while (finished != id)
                //{
                //    Thread.Sleep(100);
                //}
                result = 1;

                //if (active == false)
                //{
                //    active = true;
                //    resultTxt = "";
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        CheckPageType();
                        MyDialog md = new MyDialog(title, message);
                        md.AddMessage();
                        md.AddTextBox();
                        md.dialogButton.Click += delegate(object sender, RoutedEventArgs e)
                        {
                            //md.dialogBox.Visibility = Visibility.Collapsed;
                            result = 0;
                            resultTxt = md.textBox.Text;
                            //state = DialogStates.Free;
                            //finished++;
                            if (map != null)
                            {
                                map.disableRect.Visibility = Visibility.Collapsed;
                                map.ContentPanel.Children.Remove(md.dialogBox);
                            }
                            else if (objInfoPage != null)
                            {
                                objInfoPage.disableRect.Visibility = Visibility.Collapsed;
                                objInfoPage.ContentPanel.Children.Remove(md.dialogBox);
                            }
                        };
                        if (map != null)
                        {
                            map.disableRect.Visibility = Visibility.Visible;
                            map.ContentPanel.Children.Add(md.dialogBox);
                        }
                        else if (objInfoPage != null)
                        {
                            objInfoPage.disableRect.Visibility = Visibility.Visible;
                            objInfoPage.ContentPanel.Children.Add(md.dialogBox);
                        }
                    });
                    Console.WriteLine("PO RAZ PIERWSZY! " + resultTxt);
                    while (result == 1)
                    {
                        Thread.Sleep(100);
                    }

                //}
                //else
                //{
                //    result = 0;
                //    state = DialogStates.Free;
                //    finished++;
                //    active = false;
                //    Console.WriteLine("I PO RAZ DRUGI! " + resultTxt);
                //}

            //});
                    Console.WriteLine("ZWRACAM WARTOSC!");
            return resultTxt;
        }

        private TextBlock AddTextBlock(string message)
        {
            return new TextBlock()
            {
                Text = message,
                FontSize = 20.0,
                Padding = new Thickness(20, 5, 20, 5),
                TextWrapping = TextWrapping.Wrap,
                //Width = map.ContentPanel.ActualWidth * 0.75
            };
        }

        private TextBox AddTextBox()
        {
            return new TextBox()
            {
                FontSize = 20.0,
                Padding = new Thickness(20, 5, 20, 5),
                MinWidth = Application.Current.RootVisual.RenderSize.Width * 0.8

                //Width = map.ContentPanel.ActualWidth * 0.75
            };
        }

        private void SetDialogBoxProperties()
        {
            //map.dialogBox.Background = new SolidColorBrush((Color)Application.Current.Resources["PhoneBackgroundColor"]);
            map.dialogContent.MaxHeight = Application.Current.RootVisual.RenderSize.Height * 0.5;
            //map.disableRect.Visibility = Visibility.Visible;
            map.dialogBox.Visibility = Visibility.Visible;
        }

        //private void SetButton1Content(string s)
        //{
        //    map.dialogButton1.Content = s;
        //    map.dialogButton1.Click -= OnButton1Click;
        //    map.dialogButton1.Click += OnButton1Click;
        //}

        private void OnButton1Click(object sender, RoutedEventArgs e)
        {
            map.dialogBox.Visibility = Visibility.Collapsed;
            result = 0;
        }

        private void OnDialogButton1Click(object sender, RoutedEventArgs e)
        {
            map.dialogBox.Visibility = Visibility.Collapsed;
            //resultTxt = 
            active = false;
        }
    }

    public class MyDialog
    {
        private string title;
        private string message;
        public Button dialogButton { get; set; }
        public Border dialogBox { get; set; }
        public ListBox dialogContent { get; set; }
        public TextBox textBox { get; set; }

        public MyDialog(string t, string m)
        {
            title = t;
            message = m;
            CreateDialog();
        }

        public void CreateDialog()
        {
            dialogBox = new Border()
            {
                BorderBrush = new SolidColorBrush(Color.FromArgb((byte)255, (byte)255, (byte)255, (byte)255)),
                BorderThickness = new Thickness(3.0),
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Visibility = Visibility.Visible,
                Background = new SolidColorBrush((Color)Application.Current.Resources["PhoneBackgroundColor"])
            };

            Grid grid = new Grid();
            for (int i = 0; i < 3; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            }
            dialogBox.Child = grid;

            ScrollViewer sv = new ScrollViewer()
            {
                HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden
            };
            TextBlock dialogTitle = new TextBlock()
            {
                FontSize = 28.0,
                HorizontalAlignment = HorizontalAlignment.Center,
                Padding = new Thickness(10, 20, 10, 20),
                Text = title
            };
            sv.Content = dialogTitle;
            Grid.SetRow(sv, 0);
            grid.Children.Add(sv);

            dialogContent = new ListBox()
            {
                HorizontalAlignment = HorizontalAlignment.Center
            };
            Grid.SetRow(dialogContent, 1);
            grid.Children.Add(dialogContent);

            dialogButton = new Button()
            {
                Content = "OK"
            };
            Grid.SetRow(dialogButton, 2);
            grid.Children.Add(dialogButton);
        }

        public void AddMessage()
        {
            TextBlock tb = new TextBlock()
            {
                Text = message,
                FontSize = 20.0,
                Padding = new Thickness(20, 5, 20, 5),
                TextWrapping = TextWrapping.Wrap
            };
            dialogContent.Items.Add(tb);
        }

        public void AddTextBox()
        {
            textBox = new TextBox()
            {
                FontSize = 20.0,
                Padding = new Thickness(20, 5, 20, 5),
                MinWidth = Application.Current.RootVisual.RenderSize.Width * 0.8
            };
            dialogContent.Items.Add(textBox);
        }
    }
}
