﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GdzieJaIde.Datamodel.Enums;

namespace GdzieJaIde.Datamodel
{
    public class Argument
    {
        private ObjectType type;
        private string value;


        public Argument()
        {
        }

        public Argument(ObjectType type, string value)
        {
            this.type = type;
            this.value = value;
        }

        public string Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public ObjectType Type
        {
            get { return type; }
            set { type = value; }
        }
    }
}
