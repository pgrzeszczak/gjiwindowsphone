﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.IO.IsolatedStorage;

namespace GdzieJaIde.Datamodel
{
    public class ScenarioEntity
    {
        private int id;
        private string name;
        private string author;
        private string description;
        private string created;
        private string splashPath;
        private string scenarioPath;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string Created
        {
            get { return created; }
            set { created = value; }
        }

        public string SplashPath
        {
            get { return splashPath; }
            set { splashPath = value; }
        }

        public BitmapImage SplashImage
        {
            get
            {
                BitmapImage bi = new BitmapImage();
                using (IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = storage.OpenFile(splashPath, FileMode.Open, FileAccess.Read))
                    {
                        bi.SetSource(stream);
                    }
                }
                return bi;
            }
        }

        public string ScenarioPath
        {
            get { return scenarioPath; }
            set { scenarioPath = value; }
        }
    }
}
