﻿
using System;

namespace GdzieJaIde.Datamodel.Primitive
{
    public class StringObject : PrimitiveObject, IComparable<object>
    {
        private string value;
        
        public StringObject()
        {
            init = false;
        }

        public StringObject(int id, string idname, string value):base(id,idname)
        {
            this.value = value;
            init = true;
        }
        
        public string Value
        {
            get { return value; }
            set
            {
                bool change = this.value != value && this.value != null && init;

                this.value = value;
                init = true;

                if (change)
                {
                    if (onValueChange != null)
                    {
                        onValueChange.Invoke();
                    }
                }
            }
        }

        public int CompareTo(object other)
        {
            if (other is string)
            {
                return value.CompareTo(other);
            }
            else if (other is StringObject)
            {
                return value.CompareTo(((StringObject)other).Value);
            }
            return -1; //Exception
        }
    }
}
