﻿
using System;

namespace GdzieJaIde.Datamodel.Primitive
{
    public class DoubleObject:PrimitiveObject, IComparable<object>
    {
        private double value;
        public DoubleObject()
        {
            init = false;
        }

        public DoubleObject(int id, string idname, double value):base(id,idname)
        {
            this.value = value;
            init = true;
        }
        
        public double Value
        {
            get { return value; }
            set
            {
                bool change = this.value != value && init;
                this.value = value;
                init = true;

                if (change)
                {
                    if (onValueChange != null)
                    {
                        onValueChange.Invoke();
                    }
                }
            }
        }

        public int CompareTo(object other)
        {
            if (other is double)
            {
                return value.CompareTo(other);
            }
            else if (other is DoubleObject)
            {
                return value.CompareTo(((DoubleObject)other).Value);
            }
            return -1; //Exception
        }
    }
}
