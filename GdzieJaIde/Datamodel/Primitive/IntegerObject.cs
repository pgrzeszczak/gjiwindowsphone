﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GdzieJaIde.Datamodel.Primitive
{
    public class IntegerObject : PrimitiveObject, IComparable<object>
    {
        private int value;

        public IntegerObject()
        {
            init = false;
        }

        public IntegerObject(int id, string idname, int value):base(id,idname)
        {
            this.value = value;
            init = true;
        }

        public int Value
        {
            get { return value; }
            set
            {
                bool change = this.value != value && init;
                this.value = value;
                init = true;

                if (change)
                {
                    Console.WriteLine("jejsejrjesrjsejrfsjerfse");
                    if (onValueChange != null)
                    {
                        onValueChange.Invoke();
                    }
                }
            }
        }


        public int CompareTo(object other)
        {
            if (other is int)
            {
                return value.CompareTo(other);
            }
            else if (other is IntegerObject)
            {
                return value.CompareTo(((IntegerObject)other).Value);
            }
            return -1; //Exception
        }
    }
}
