﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GdzieJaIde.Datamodel.Primitive
{
    public abstract class PrimitiveObject:SimpleObject
    {
        public EventDelegate.Event onValueChange;
        public static string ON_VALUE_CHANGE="onValueChange";
        protected bool init;

        public PrimitiveObject()
        {
        }

        public PrimitiveObject(int id, string idname):base(id,idname)
        {
            ;
        }
    }
}
