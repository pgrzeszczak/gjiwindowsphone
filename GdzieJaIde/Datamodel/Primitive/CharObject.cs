﻿
using System;

namespace GdzieJaIde.Datamodel.Primitive
{
    public class CharObject : PrimitiveObject, IComparable<object>
    {
        private char value;

        public CharObject()
        {
            init = false;
        }

        public CharObject(int id, string idname, char value):base(id,idname)
        {
            this.value = value;
        }
        
        public char Value
        {
            get { return value; }
            set
            {
                bool change = this.value != value && init;
                this.value = value;

                if (change)
                {
                    if (onValueChange != null)
                    {
                        onValueChange.Invoke();
                    }
                }
            }
        }

        public int CompareTo(object other)
        {
            if (other is char)
            {
                return value.CompareTo(other);
            }
            else if (other is CharObject)
            {
                return value.CompareTo(((CharObject)other).Value);
            }
            return -1; //Exception
        }
    }
}
