﻿
using System;

namespace GdzieJaIde.Datamodel.Primitive
{
    public class BooleanObject:PrimitiveObject, IComparable<object>
    {
        private bool value;
        public BooleanObject()
        {
            init = false;
        }

        public BooleanObject(int id, string idname, bool value):base(id,idname)
        {
            this.value = value;
            init = true;
        }
        
        public bool Value
        {
            get { return value; }
            set
            {
                bool change = this.value != value && init;
                this.value = value;

                if(change)
                {
                    if(onValueChange!=null)
                    {
                        onValueChange.Invoke();
                    }
                }
            }
        }


        public int CompareTo(object other)
        {
            if(other is bool)
            {
                return value.CompareTo(other);
            }
            else if(other is BooleanObject)
            {
                return value.CompareTo(((BooleanObject) other).Value);
            }
            return -1; //Exception
        }
    }
}
