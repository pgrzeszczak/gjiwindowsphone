﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GdzieJaIde.Datamodel.Exceptions
{
    public class NodeStructureException:ParseException
    {
        private string nodeName;
        private string parentNodeName;

        public NodeStructureException(string nodeName, string parentNodeName)
        {
            this.nodeName = nodeName;
            this.parentNodeName = parentNodeName;
        }

        public string ParentNodeName
        {
            get { return parentNodeName; }
            set { parentNodeName = value; }
        }

        public string NodeName
        {
            get { return nodeName; }
            set { nodeName = value; }
        }

        public override string Message
        {
            get
            {
                return "Structure violation for node "+nodeName+" parent node "+parentNodeName+" "+base.Message;
            }
        }
    }
}
