﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GdzieJaIde.Datamodel.Exceptions
{
    public class NullAttributeValueException: ParseException
    {
        private string attributeName;
        private string nodeName;

        public NullAttributeValueException(string attributeName, string nodeName)
        {
            this.attributeName = attributeName;
            this.NodeName = nodeName;
        }

        public override string Message
        {
            get
            {
                return "Null value for attribute "+attributeName+"\n"+base.Message;
            }
        }

        public string AttributeName
        {
            get { return attributeName; }
            set { attributeName = value; }
        }

        public string NodeName
        {
            get { return nodeName; }
            set { nodeName = value; }
        }
    }
}
