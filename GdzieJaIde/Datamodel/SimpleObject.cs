﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GdzieJaIde.Datamodel
{
    public abstract class SimpleObject
    {
        protected int id;
        protected string idname;

        public SimpleObject()
        {
        }

        public SimpleObject(int id, string idname)
        {
            this.id = id;
            this.idname = idname;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Idname
        {
            get { return idname; }
            set { idname = value; }
        }
    }
}
