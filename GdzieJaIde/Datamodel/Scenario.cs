﻿using System;

namespace GdzieJaIde.Datamodel
{
    public class Scenario:SimpleObject
    {
        public const string ON_BEGIN = "onBegin";
        public const string ON_END = "onEnd";
        public const string ON_RESTORE = "onRestore";
        public const string ON_SAVE = "onSave";

        private String name;
        private String description;
        private String author;
        private String creatorVersion;
        private String version;
        private String created;

        public EventDelegate.Event onBegin;
        public EventDelegate.Event onEnd;
        public EventDelegate.Event onRestore;
        public EventDelegate.Event onSave;

        public Scenario()
        {
            onBegin = delegate { };
            onEnd = delegate { };
            onRestore = delegate { };
            onSave = delegate { };
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        public string CreatorVersion
        {
            get { return creatorVersion; }
            set { creatorVersion = value; }
        }

        public string Version
        {
            get { return version; }
            set { version = value; }
        }

        public string Created
        {
            get { return created; }
            set { created = value; }
        }

        public override string ToString()
        {
            //return "Author " + author + "\ncreated " + created + "\ndescription " + description + "\nname " + name +
            //       "\nversion " + version + "\ncreatorVersion " + creatorVersion;

            return String.Format("Author: {0}\nCreated: {1}\nDescription: {2}\nVersion: {3}\nCreator Ver.:{4}", author, created, description, version, creatorVersion);
        }

}
}
