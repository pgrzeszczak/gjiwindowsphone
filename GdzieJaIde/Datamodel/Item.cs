﻿namespace GdzieJaIde.Datamodel
{
    public class Item: SimpleObject
    {

        public const string ON_FIND = "onFind";
        public const string ON_DROP = "onDrop";
        public const string ON_PICK = "onPick";
        public const string ON_STATE_CHANGE = "onStateChange";
        public const string ON_VISIBLE_CHANGE = "onVisibleChange";
        public const string ON_ZONE_CHANGE = "onZoneChange";

        private string name;
        private string description;
        private int zone;
        private int icon;
        private string state;
        private bool visible;

        public EventDelegate.Event onVisibleChange;
        public EventDelegate.Event onStateChange;
        public EventDelegate.Event onZoneChange;
        public EventDelegate.Event onFind;
        public EventDelegate.Event onDrop;
        public EventDelegate.Event onPick;
        
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public int Zone
        {
            get { return zone; }
            set
            {
                bool change = zone != value;
                zone = value;
                if (change && onZoneChange != null)
                {
                    onZoneChange.Invoke();
                }
            }
        }

        public string State
        {
            get { return state; }
            set
            {
                bool change = state != value;
                state = value;
                if (change && onStateChange != null)
                {
                    onStateChange.Invoke();
                }
            }
        }

        public bool Visible
        {
            get { return visible; }
            set
            {
                bool change = visible != value;
                visible = value;
                if (change && onVisibleChange != null)
                {
                    onVisibleChange.Invoke();
                }
            }
        }

        public int Icon
        {
            get { return icon; }
            set { icon = value; }
        }
    }
}
