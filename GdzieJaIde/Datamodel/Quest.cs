﻿namespace GdzieJaIde.Datamodel
{
    public class Quest:SimpleObject
    {

        public const string ON_COMPLETE = "onComplete";
        public const string ON_BEGIN = "onBegin";
        public const string ON_INTERACTION = "onInteraction";
        public const string ON_STATE_CHANGE = "onStateChange";
        public const string ON_VISIBLE_CHANGE = "onVisibleChange";

        private string name;
        private string description;
        private string state;
        private bool visible;
        private int icon;

        public EventDelegate.Event onComplete;
        public EventDelegate.Event onBegin;
        public EventDelegate.Event onInteraction;
        public EventDelegate.Event onStateChange;
        public EventDelegate.Event onVisibleChange;
        
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string State
        {
            get { return state; }
            set
            {
                bool change = state != value;
                state = value;
                if (change && onStateChange != null)
                {
                    onStateChange.Invoke();
                }
            }
        }

        public bool Visible
        {
            get { return visible; }
            set
            {
                bool change = visible != value;
                visible = value;
                if (change && onVisibleChange != null)
                {
                    onVisibleChange.Invoke();
                }
            }
        }

        public int Icon
        {
            get { return icon; }
            set { icon = value; }
        }
    }
}
