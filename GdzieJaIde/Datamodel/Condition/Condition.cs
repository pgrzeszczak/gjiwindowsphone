﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GdzieJaIde.API;
using GdzieJaIde.Datamodel.Enums;
using GdzieJaIde.Datamodel.Condition;
using GdzieJaIde.Datamodel.Primitive;

namespace GdzieJaIde.Datamodel.Condition
{
    public class Condition:ICondition
    {
        public enum OperatorType{EQUAL,GREATER,LESS,NOTEQUAL, GREATEREQUAL, LESSEQUAL};

        private OperatorType op;
        private ObjectType eastSideType;
        private ObjectType westSideType;
        private string eastSideValue;
        private string westSideValue;

        public OperatorType Op
        {
            get { return op; }
            set { op = value; }
        }

        public ObjectType EastSideType
        {
            get { return eastSideType; }
            set { eastSideType = value; }
        }

        public ObjectType WestSideType
        {
            get { return westSideType; }
            set { westSideType = value; }
        }

        public string EastSideValue
        {
            get { return eastSideValue; }
            set { eastSideValue = value; }
        }

        public string WestSideValue
        {
            get { return westSideValue; }
            set { westSideValue = value; }
        }

      

        public bool test()
        {
            /*if(eastSideType!=WestSideType)
            {
                return false;
            }*/
            object eVal = UTILS.getValue(eastSideType, eastSideValue);
            object wVal = UTILS.getValue(westSideType, westSideValue);
            int result = ((IComparable)eVal).CompareTo(wVal);
          /*  if(eastSideValue is IComparable<object>)
            {
                result = ((IComparable<object>)eastSideValue).CompareTo(westSideValue);
                
            }
            else
            {
                if(westSideValue is PrimitiveObject)
                {
                    result = ((IComparable)eastSideValue).CompareTo(UTILS.getValue((PrimitiveObject)westSideValue));
                }
                else if(westSideValue is SimpleObject)
                {
                    result = ((IComparable)eastSideValue).CompareTo(UTILS.getValue((SimpleObject)westSideValue, fieldname));
                }
                else
                {
                    result = ((IComparable)eastSideValue).CompareTo((IComparable)westSideValue);
                }
            }*/
            
            switch (Op)
            {
              case OperatorType.LESS:
                    return result < 0;
              case OperatorType.EQUAL:
                    return result == 0;
              case OperatorType.NOTEQUAL:
                    return result != 0;
              case OperatorType.GREATER:
                    return result > 0;
              case OperatorType.GREATEREQUAL:
                    return result >= 0;
              case OperatorType.LESSEQUAL:
                    return result <= 0;

            }
            return false;
        }
    }
}
