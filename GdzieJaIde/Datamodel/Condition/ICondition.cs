﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GdzieJaIde.Datamodel.Condition
{
    public interface ICondition
    {
        bool test();
    }
}
