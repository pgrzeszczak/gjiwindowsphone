﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections;

namespace GdzieJaIde.Datamodel.Condition
{
    public class ComplexCondition:ICondition
    {
        public enum ConditionType
        {
            AND,OR
        }

        private ConditionType type;

        private readonly List<ICondition> conditions;

        public ComplexCondition()
        {
            conditions = new List<ICondition>();
        }

        public ConditionType Type
        {
            get { return type; }
            set { type = value; }
        }

        public void addCondition(ICondition condition)
        {
            if (condition == null) throw new ArgumentNullException("condition");
            conditions.Add(condition);
        }

        public bool test()
        {
            bool result=true;
            if(Type==ConditionType.AND)
            {
                foreach(ICondition condition in conditions)
                {
                    result = result && condition.test();
                }
            }
            else
            {
                foreach (ICondition condition in conditions)
                {
                    if(condition.test())
                    {
                        return true;
                    }
                }
                result= false;
            }
            return result;
        }
    }
}
