﻿
namespace GdzieJaIde.Datamodel
{
    public class Media:SimpleObject
    {
        public const string ON_PLAY = "onPlay";
        public const string ON_SHOW = "onShow";

        private MediaType type;
        private string path;
        private int icon;

        public EventDelegate.Event onPlay;
        public EventDelegate.Event onShow;

        public MediaType Type
        {
            get { return type; }
            set { type = value; }
        }

        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        public int Icon
        {
            get { return icon; }
            set { icon = value; }
        }
    }
}
