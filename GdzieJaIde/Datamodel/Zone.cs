﻿using System.Collections.Generic;
using System.Windows;

namespace GdzieJaIde.Datamodel
{
    public class Zone:SimpleObject
    {
        public const string ON_ENTER = "onEnter";
        public const string ON_LEAVE = "onLeave";
        public const string ON_STATE_CHANGE = "onStateChange";
        public const string ON_VISIBLE_CHANGE = "onVisibleChange";

        private string name;
        private string description;
        private double radius;
        private string state;
        private bool visible;
        private List<Point> points;
        private int icon;

        public EventDelegate.Event onVisibleChange;
        public EventDelegate.Event onStateChange;
        public EventDelegate.Event onEnter;
        public EventDelegate.Event onLeave;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public double Radius
        {
            get { return radius; }
            set { radius = value; }
        }

        public string State
        {
            get { return state; }
            set
            {
                bool change = state != value;
                state = value;
                if (change && onStateChange != null)
                {
                    onStateChange.Invoke();
                }
            }
        }

        public bool Visible
        {
            get { return visible; }
            set
            {
                bool change = visible != value;
                visible = value;
                if (change && onVisibleChange != null)
                {
                    onVisibleChange.Invoke();
                }
            }
        }

        public List<Point> Points
        {
            get { return points; }
            set { points = value; }
        }

        public int Icon
        {
            get { return icon; }
            set { icon = value; }
        }

        public override string ToString()
        {
            
            return "ID "+id+"\nName "+name+"" + "\nDescription "+description +"\nRadius "+
                radius+"\nState "+state+"\nVisible "+visible+ "\nPoints "+points;
        }
    }
}
