﻿using System;

namespace GdzieJaIde.Datamodel
{
    public class Timer:SimpleObject
    {

        public const string ON_TIMER = "onTimer";
        public const string ON_STOP = "onStop";
        public const string ON_START = "onStart";

        private long interval;
        private int icon;
        private System.Windows.Threading.DispatcherTimer dispatcherTimer;

        public EventDelegate.Event onTimer;
        public EventDelegate.Event onStop;
        public EventDelegate.Event onStart;

        public long Interval
        {
            get { return interval; }
            set { interval = value; }
        }

        public int Icon
        {
            get { return icon; }
            set { icon = value; }
        }

        public void StartTimer()
        {
            if (onStart != null)
            {

                onStart.Invoke();
            }
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, (int) interval, 0);
            
            dispatcherTimer.Start();
            dispatcherTimer.Tick += OnTimerTick;
        }
        void OnTimerTick(Object sender, EventArgs args)
        {
            if (onTimer != null)
            {

                onTimer.Invoke();
            }
        }
        public void StopTimer()
        {
            dispatcherTimer.Stop();
            if (onStop != null)
            {
                onStop.Invoke();
            }
        }
    }
}
