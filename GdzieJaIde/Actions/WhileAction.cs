﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GdzieJaIde.Datamodel;
using GdzieJaIde.Datamodel.Condition;

namespace GdzieJaIde.Actions
{
    public class WhileAction : ISimpleAction
    {
        private ICondition condition;
        private EventDelegate.Event body;

        public WhileAction()
        {
            body=new EventDelegate.Event(delegate { ; });
        }

        public ICondition Condition
        {
            set { condition = value; }
        }

        public void invoke()
        {
            while (condition.test())
            {
                body.Invoke();
            }
        }

        public void addActionToBody(EventDelegate.Event action)
        {
            body += action;
        }
    }
}
