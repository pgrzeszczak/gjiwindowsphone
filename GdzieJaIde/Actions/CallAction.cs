﻿using System;
using GdzieJaIde.API;
using GdzieJaIde.Datamodel;
using GdzieJaIde.Datamodel.Primitive;

namespace GdzieJaIde.Actions
{
    public class CallAction: ISimpleAction
    {
        private object[] valuesOfArguments;
        private Argument[] arguments;
        private string functionName;
        private ClassType classType;
        private Argument target;

        public static int id;

        public Argument[] Arguments
        {
            set { arguments = value;
            valuesOfArguments = new object[arguments.Length];
            }
        }

        public string FunctionName
        {
            set { functionName = value; }
        }

        public ClassType ClassName
        {
            set { classType = value; }
        }

        public Argument Target
        {
            get { return target; }
            set { target = value; }
        }

        private void updateValues()
        {
            arguments.CopyTo(valuesOfArguments,0);
            for (int i = 0; i < arguments.Length; i++)
            {
                valuesOfArguments[i] = UTILS.getValue(arguments[i].Type,arguments[i].Value);
            }
        }


        public void invoke()
        {
            updateValues();
            object targetValue = null;
            switch (classType)
            {
                case ClassType.GUI:
                    if (functionName==GUI.CHOICE)
                    {
                        targetValue = GUI.choice(valuesOfArguments as string[]);
                    }
                    else if (functionName==GUI.CONFIRM)
                    {
                        targetValue = GUI.confirm(valuesOfArguments[0] as string, valuesOfArguments[1] as string);
                    }
                    else if (functionName==GUI.DIALOG)
                    {

                        targetValue = GUI.dialog(valuesOfArguments[0] as string, valuesOfArguments[1] as string);
                    }
                    else if (functionName==GUI.MESSAGE)
                    {
                        GUI.message(valuesOfArguments[0] as string, valuesOfArguments[1] as string);
                    }
                    else if (functionName.Equals(GUI.SHOW_MEDIA))
                    {
                        GUI.showMedia(valuesOfArguments[0] is int ? (int) valuesOfArguments[0] : -1);
                    }
                    break;
                case ClassType.STRING:
                    if (functionName == STRING.APPEND)
                    {
                        targetValue = STRING.append(valuesOfArguments[0] as string, valuesOfArguments[1] as string);
                    }
                    else if(functionName==STRING.CONTAINS)
                    {
                        targetValue = STRING.contains(valuesOfArguments[0] as string, valuesOfArguments[1] as string);
                    }
                    else if(functionName==STRING.REMOVE)
                    {
                        targetValue = STRING.remove(valuesOfArguments[0] as string, valuesOfArguments[1] as string);
                    }
                    else if(functionName==STRING.REPLACE)
                    {
                        targetValue = STRING.replace(valuesOfArguments[0] as string, valuesOfArguments[1] as string, valuesOfArguments[2] as string);
                    }
                    break;
                case ClassType.MATH:
                    if (functionName == MATH.ABS)
                    {
                        if (valuesOfArguments[0] is int)
                        {
                            targetValue = MATH.abs((int)valuesOfArguments[0]);
                        }
                        else if (valuesOfArguments[0] is double)
                        {
                            targetValue = MATH.abs((double)valuesOfArguments[0]);
                        }
                    }
                    else if (functionName == MATH.ADD)
                    {
                        if (valuesOfArguments[0] is int)
                        {
                            targetValue = MATH.add((int)valuesOfArguments[0], (int)valuesOfArguments[1]);
                        }
                        else if (valuesOfArguments[0] is double)
                        {
                            targetValue = MATH.add((double)valuesOfArguments[0], (double)valuesOfArguments[1]);
                        }
                    }
                    else if (functionName == MATH.DIV)
                    {
                        if (valuesOfArguments[0] is int)
                        {
                            targetValue = MATH.div((int)valuesOfArguments[0], (int)valuesOfArguments[1]);
                        }
                        else if (valuesOfArguments[0] is double)
                        {
                            targetValue = MATH.div((double)valuesOfArguments[0], (double)valuesOfArguments[1]);
                        }
                    }
                    else if (functionName == MATH.MAX)
                    {
                        if (valuesOfArguments[0] is int)
                        {
                            targetValue = MATH.max((int)valuesOfArguments[0], (int)valuesOfArguments[1]);
                        }
                        else if (valuesOfArguments[0] is double)
                        {
                            targetValue = MATH.max((double)valuesOfArguments[0], (double)valuesOfArguments[1]);
                        }
                    }
                    else if (functionName == MATH.MIN)
                    {
                        if (valuesOfArguments[0] is int)
                        {
                            targetValue = MATH.min((int)valuesOfArguments[0], (int)valuesOfArguments[1]);
                        }
                        else if (valuesOfArguments[0] is int)
                        {
                            targetValue = MATH.min((double)valuesOfArguments[0], (double)valuesOfArguments[1]);
                        }
                    }
                    else if (functionName == MATH.MINUS)
                    {
                        if (valuesOfArguments[0] is int)
                        {
                            targetValue = MATH.minus((int)valuesOfArguments[0], (int)valuesOfArguments[1]);
                        }
                        else if (valuesOfArguments[0] is int)
                        {
                            targetValue = MATH.minus((double)valuesOfArguments[0], (double)valuesOfArguments[1]);
                        }
                    }
                    else if (functionName == MATH.MOD)
                    {
                        if (valuesOfArguments[0] is int)
                        {
                            targetValue = MATH.mod((int)valuesOfArguments[0], (int)valuesOfArguments[1]);
                        }
                    }
                    else if (functionName == MATH.MULTI)
                    {
                        if (valuesOfArguments[0] is int)
                        {
                            targetValue = MATH.multi((int)valuesOfArguments[0], (int)valuesOfArguments[1]);
                        }
                        else if (valuesOfArguments[0] is double)
                        {
                            targetValue = MATH.multi((double)valuesOfArguments[0], (double)valuesOfArguments[1]);
                        }
                    }
                    else if (functionName == MATH.POWER)
                    {
                        if (valuesOfArguments[0] is int)
                        {
                            targetValue = MATH.power((int)valuesOfArguments[0], (int)valuesOfArguments[1]);
                        }
                        else if (valuesOfArguments[0] is double)
                        {
                            targetValue = MATH.power((double)valuesOfArguments[0], (double)valuesOfArguments[1]);
                        }
                    }
                    else if (functionName == MATH.ROOT)
                    {
                        if (valuesOfArguments[0] is double)
                        {
                            targetValue = MATH.root((double)valuesOfArguments[0], (double)valuesOfArguments[1]);
                        }
                    }
                    else if (functionName == MATH.ROUND)
                    {
                        if (valuesOfArguments[0] is double)
                        {
                            targetValue = MATH.round((double)valuesOfArguments[0]);
                        }
                    }
                    else if (functionName == MATH.SUM)
                    {
                        if (valuesOfArguments[0] is int)
                        {
                            int[] array = new int[valuesOfArguments.Length];
                            valuesOfArguments.CopyTo(array, 0);
                            targetValue = MATH.sum(array);
                        }
                        if (valuesOfArguments[0] is double)
                        {
                            double[] array = new double[valuesOfArguments.Length];
                            valuesOfArguments.CopyTo(array, 0);
                            targetValue = MATH.sum(array);
                        }
                    }
                    break;
                case ClassType.UTILS:
                    if (functionName == UTILS.CHANGE_CHARACTER_VISIBIITY)
                    {
                        UTILS.changeCharacterVisibility((int)valuesOfArguments[0], (bool)valuesOfArguments[1]);
                    }
                    else if (functionName == UTILS.CHANGE_ITEM_VISIBIITY)
                    {
                        UTILS.changeItemVisibility((int)valuesOfArguments[0], (bool)valuesOfArguments[1]);
                    }
                    else if (functionName == UTILS.CHANGE_QUEST_VISIBIITY)
                    {
                        UTILS.changeQuestVisibility((int)valuesOfArguments[0], (bool)valuesOfArguments[1]);
                    }
                    else if (functionName == UTILS.CHANGE_ZONE_VISIBIITY)
                    {
                        UTILS.changeZoneVisibility((int)valuesOfArguments[0], (bool)valuesOfArguments[1]);
                    }
                    else if (functionName == UTILS.CHANGE_CHARACTER_ZONE)
                    {
                        UTILS.changeCharacterZone((int)valuesOfArguments[0], (int)valuesOfArguments[1]);
                    }
                    else if (functionName == UTILS.CHANGE_ITEM_ZONE)
                    {
                        UTILS.changeItemZone((int)valuesOfArguments[0], (int)valuesOfArguments[1]);
                    }
                    else if (functionName == UTILS.START_TIMER)
                    {
                        UTILS.startTimer((int)valuesOfArguments[0]);
                    }
                    else if (functionName == UTILS.STOP_TIMER)
                    {
                        UTILS.stopTimer((int)valuesOfArguments[0]);
                    }
                    else if (functionName == UTILS.SET)
                    {
                        if (valuesOfArguments.Length == 1)
                        {
                            targetValue = UTILS.set(valuesOfArguments[0]);

                        }
                    }
                    else if (functionName == UTILS.STRING_TO_DOUBLE)
                    {
                        
                            targetValue = UTILS.stringToDouble((string) valuesOfArguments[0]);

                        
                    }
                    else if (functionName == UTILS.STRING_TO_INT)
                    {
                        if (valuesOfArguments.Length == 1)
                        {
                            targetValue = UTILS.stringToInt((string) valuesOfArguments[0]);

                        }
                    }
                    break;
            }

            if (target != null){
                UTILS.setValue(target.Type, target.Value, targetValue);
            }
        }
    }
}
