﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GdzieJaIde.Datamodel;

namespace GdzieJaIde.Actions
{
    public class ForAction : ISimpleAction
    {

        private int starts;
        private int ends;
        private int step;

        private EventDelegate.Event body;

        public int Starts
        {
            get { return starts; }
            set { starts = value; }
        }

        public int Ends
        {
            get { return ends; }
            set { ends = value; }
        }

        public int Step
        {
            get { return step; }
            set { step = value; }
        }

        public ForAction()
        {
            body=new EventDelegate.Event(() => {});
        }
        public void invoke()
        {
            for(int i=starts;i<ends;i+=step)
            {
                body.Invoke();
            }
        }

        public void addActionToBody(EventDelegate.Event action)
        {
            body += action;

        }
    }
}
