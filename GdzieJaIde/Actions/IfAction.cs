﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GdzieJaIde.Datamodel;
using GdzieJaIde.Datamodel.Condition;

namespace GdzieJaIde.Actions
{
    public class IfAction : ISimpleAction
    {
        private ICondition condition;

        private EventDelegate.Event trueBody;
        private EventDelegate.Event falseBody;

        public IfAction()
        {
            trueBody = new EventDelegate.Event(delegate {  });
            falseBody = new EventDelegate.Event(delegate {  });
        }

        public ICondition Condition
        {
            get { return condition; }
            set { condition = value; }
        }


        public void invoke()
        {
            if(condition.test())
            {
                trueBody.Invoke();
            }
            else
            {
                falseBody.Invoke();
            }
        }
        public void addActionToTrueBody(EventDelegate.Event action)
        {
            trueBody += action;

        }
        public void addActionToFalseBody(EventDelegate.Event action)
        {
            falseBody += action;

        }
    }
}
