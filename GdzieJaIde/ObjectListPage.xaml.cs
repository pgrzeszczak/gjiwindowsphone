﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Device.Location;
using GdzieJaIde.Core;
using GdzieJaIde.Datamodel;

namespace GdzieJaIde
{
    public partial class ObjectListPage : PhoneApplicationPage
    {
        private ScenarioObject _objectType;

        //private List<Zone> _zones;
        //private List<Item> _items;
        //private List<Item> _bag;
        //private List<Character> _characters;
        //private List<Quest> _quests;

        private List<SimpleObject> _objects;
        private int _currentZone;
        private List<BitmapImage> _icons;

        private MainControl Data
        {
            get
            {
                return MainControl.getInstance();
            }
        }

        public ObjectListPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            IDictionary<string, string> parameters = this.NavigationContext.QueryString;
            if (parameters.ContainsKey("Type"))
            {
                _objectType = (ScenarioObject)Enum.Parse(typeof(ScenarioObject), parameters["Type"], true);
                PageTitle.Text = String.Format("{0}s", parameters["Type"].ToLower());
            }
            if (parameters.ContainsKey("CurrZone"))
            {
                _currentZone = Int32.Parse(parameters["CurrZone"]);
            }

            _objects = new List<SimpleObject>();
            _icons = new List<BitmapImage>();
            FillList();
            ObjectsList.ItemsSource = _objects;

            base.OnNavigatedTo(e);
        }

        private void FillList()
        {
            if (_objectType == ScenarioObject.Zone)
            {
                foreach (int id in Data.getZonesList())
                {
                    if (Data.getZone(id).Visible == true)
                    {
                        _objects.Add(Data.getZone(id));
                    }
                }    
            }
            else if (_objectType == ScenarioObject.Item)
            {
                foreach (int id in Data.getItemsList())
                {
                    if (Data.getItem(id).Visible == true && _currentZone == Data.getItem(id).Zone)
                    {
                        _objects.Add(Data.getItem(id));
                    }
                }
            }
            else if (_objectType == ScenarioObject.Character)
            {
                foreach (int id in Data.getCharactersList())
                {
                    if (Data.getCharacter(id).Visible == true && _currentZone == Data.getCharacter(id).Zone)
                    {
                        _objects.Add(Data.getCharacter(id));
                    }
                }
            }
            else if (_objectType == ScenarioObject.Quest)
            {
                foreach (int id in Data.getQuestList())
                {
                    if (Data.getQuest(id).Visible == true)
                    {
                        _objects.Add(Data.getQuest(id));
                    }
                }
            }
        }

        private void Grid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int id = (ObjectsList.SelectedItem as SimpleObject).Id;

            if (_objectType == ScenarioObject.Zone)
            {
                double lon, lat;
                Map map = (Application.Current as App).MapPage;
                map.CalculateCenterPoint(id, out lat, out lon);

                map.googlemap.Center = new GeoCoordinate(lat, lon);
                NavigationService.GoBack();
            }
            else if (_objectType == ScenarioObject.Quest)
            {
                ObjectsList.SelectedIndex = -1;
            }
            else //if (_objectType != ScenarioObject.Zone)
            {
                string type = System.Enum.GetName(typeof(ScenarioObject), _objectType);
                string destination = String.Format("/ObjectInfoPage.xaml?Type={0}&Id={1}", type, id);

                NavigationService.Navigate(new Uri(destination, UriKind.Relative));
            }
        }
    }

    public enum ScenarioObject
    {
        Zone, Item, Character, Bag, Quest
    }
}