﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GdzieJaIde.API
{
    public class STRING
    {
        public const string APPEND = "append";
        public const string CONTAINS = "contains";
        public const string REMOVE = "remove";
        public const string REPLACE = "replace";
        public static string append(string a, string b)
        {
            return a + b;
        }

        public static bool contains(string a, string b)
        {
            return a.Contains(b);
        }

        public static string remove(string a, string b)
        {
            return a.Replace(b, "");
        }

        public static string replace(string a, string b, string c)
        {
            return a.Replace(b, c);
        }

    }
}
