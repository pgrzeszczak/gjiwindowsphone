﻿using System;
using System.Text.RegularExpressions;
using GdzieJaIde.Core;
using GdzieJaIde.Datamodel;
using GdzieJaIde.Datamodel.Enums;
using GdzieJaIde.Datamodel.Primitive;

namespace GdzieJaIde.API
{
    public class UTILS
    {
        private static MainControl CONTROL=MainControl.getInstance();

        public const string CHANGE_CHARACTER_VISIBIITY = "changeCharacterVisibility";
        public const string CHANGE_ITEM_VISIBIITY = "changeItemVisibility";
        public const string CHANGE_QUEST_VISIBIITY = "changeQuestVisibility";
        public const string CHANGE_ZONE_VISIBIITY = "changeZoneVisibility";
        public const string CHANGE_CHARACTER_ZONE = "changeCharacterZone";
        public const string CHANGE_ITEM_ZONE = "changeItemZone";
        public const string ITEM_ZONE = "changeItemZone";
        public const string GET_VALUE = "changeGetValue";
        public const string SET = "set";
        public const string START_TIMER = "startTimer";
        public const string STOP_TIMER = "stopTimer";
        public const string STRING_TO_INT= "stringToInt";
        public const string STRING_TO_DOUBLE = "stringToDouble";

        public static object set(object a)
        {
            return a;
        }

        public static void changeZoneVisibility(int ZoneID, bool visible){

            Object obj = CONTROL.getObject(ZoneID);
            if(obj is Zone)
            {
                ((Zone) obj).Visible=visible;
            };
        }

        public static void changeCharacterVisibility(int CharacterID, bool visible)
        {
            Object obj = CONTROL.getObject(CharacterID);
            if (obj is Character)
            {
                ((Character)obj).Visible = visible;
            };

        }

        public static void changeItemVisibility(int ItemID, bool visible)
        {
            Object obj = CONTROL.getObject(ItemID);
            if (obj is Item)
            {
                ((Item)obj).Visible = visible;
            };

        }

        public static void changeQuestVisibility(int QuestID, bool visible)
        {
            Object obj = CONTROL.getObject(QuestID);
            if(obj is Quest)
            {
                ((Quest) obj).Visible=visible;
            };

        }

        public static void changeCharacterZone(int CharacterID, int ZoneID)
        {
            Object character = CONTROL.getObject(CharacterID);
            if (character is Character)
            {
                ((Character)character).Zone =  ZoneID;
            };


        }

        public static void changeItemZone(int ItemID, int ZoneID)
        {
            Object item = CONTROL.getObject(ItemID);
            if (item is Item)
            {
                ((Item)item).Zone = ZoneID;
            };

        }

        public static void startTimer(int TimerID)
        {

            object obj = CONTROL.getObject(TimerID);
            if (obj is Timer)
            {
                ((Timer)obj).StartTimer();
            }

        }

        public static void stopTimer(int TimerID)
        {
            object obj = CONTROL.getObject(TimerID);
            if (obj is Timer)
            {
                ((Timer)obj).StopTimer();
            }
        }

        public static object getValue(int objectId)
        {
            object obj = CONTROL.getObject(objectId);
            object val = null;
            if (obj is BooleanObject)
            {
                val = ((BooleanObject)obj).Value;
            }
            else if (obj is CharObject)
            {
                val = ((CharObject)obj).Value;
            }
            else if (obj is DoubleObject)
            {
                val = ((DoubleObject)obj).Value;
            }
            else if (obj is IntegerObject)
            {
                val = ((IntegerObject)obj).Value;
            }
            else if (obj is StringObject)
            {
                val = ((StringObject)obj).Value;
            }
            return val;
        }

        public static object getValue(PrimitiveObject obj)
        {
            object val = null;
            if (obj is BooleanObject)
            {
                val = ((BooleanObject)obj).Value;
            }
            else if (obj is CharObject)
            {
                val = ((CharObject)obj).Value;
            }
            else if (obj is DoubleObject)
            {
                val = ((DoubleObject)obj).Value;
            }
            else if (obj is IntegerObject)
            {
                val = ((IntegerObject)obj).Value;
            }
            else if (obj is StringObject)
            {
                val = parseString(((StringObject)obj).Value);
            }
            return val;
        }


        public static object getValue(ObjectType type, string value)
        {
            switch (type)
            {
                case ObjectType.BOOLEAN:
                    return Convert.ToBoolean(value);
                case ObjectType.DOUBLE:
                    return Convert.ToDouble(value);
                case ObjectType.CHAR:
                    return Convert.ToChar(value);
                case ObjectType.INTEGER:
                    return Convert.ToInt32(value);
                case ObjectType.STRING:
                    return parseString(value);;
                case ObjectType.VALUE:
                    string objID = value;
                    string field = null;
                    if (objID.Contains("."))
                    {
                        string[] afterSplit = objID.Split('.');
                        objID = afterSplit[0];
                        field = afterSplit[1];
                        field = field.Substring(0, 1).ToUpper() + field.Substring(1);
                    }
                    try
                    {
                        SimpleObject obj = (SimpleObject) CONTROL.getObject(Convert.ToInt32(objID));
                        if (obj is PrimitiveObject)
                        {
                            return getValue((PrimitiveObject) obj);
                        }
                        if (obj != null)
                        {
                            var fieldInfo = obj.GetType().GetProperty(field);
                            if (fieldInfo != null)
                            {
                                return fieldInfo.GetGetMethod().Invoke(obj, null);
                            }
                        }
                    }
                    catch (FormatException ex)
                    {
                        Console.Out.WriteLine("[WARNING} Unknow id " + value);
                        return "";
                    }

                    break;
            }
            throw new ArgumentException("Unknow type " + value + type.ToString());
        }


      /*  public static object getValue(SimpleObject obj, string fieldname)
        {
            object value = null;
            if (obj is BooleanObject)
            {
                value = ((BooleanObject)obj).value;
            }
            else if (obj is CharObject)
            {
                value = ((CharObject)obj).value;
            }
            else if (obj is DoubleObject)
            {
                value = ((DoubleObject)obj).value;
            }
            else if (obj is IntegerObject)
            {
                value = ((IntegerObject)obj).value;
            }
            else if (obj is StringObject)
            {
                value = ((StringObject)obj).value;
            }
            return value;
        }*/

        public static void setValue(ObjectType type, string value, object targetValue)
        {

            string id = value, field = null;
            if (value.Contains("."))
            {
                string[] afterSplit = value.Split('.');
                id = afterSplit[0];
                field = afterSplit[1];
                field = field.Substring(0, 1).ToUpper() + field.Substring(1);
            }
            else
            {
                object obj = CONTROL.getObject(Convert.ToInt32(id));
                if (obj is BooleanObject)
                {
                    ((BooleanObject) obj).Value = (bool) targetValue;
                }
                else if (obj is CharObject)
                {
                    ((CharObject) obj).Value = (char) targetValue;
                }
                else if (obj is DoubleObject)
                {
                    ((DoubleObject) obj).Value = (double) targetValue;
                }
                else if (obj is IntegerObject)
                {
                    ((IntegerObject) obj).Value = (int) targetValue;
                }
                else if (obj is StringObject)
                {
                    ((StringObject) obj).Value = (string) targetValue;
                }
                return;
            }

            object targetObj = CONTROL.getObject(Convert.ToInt32(id));

            var fieldInfo = targetObj.GetType().GetProperty(field);
            if (fieldInfo !=null){
                object[] args = {targetValue};

                fieldInfo.GetSetMethod().Invoke(targetObj, args);
            }
            else
            {
                throw new ArgumentException("Unknow field " + value + type.ToString());
            }
        }
        private static string parseString(string p)
        {
            string value = p;
            if (p.Contains("{{{") && p.Contains("}}}"))
            {
                Regex r = new Regex("{{{([a-zA-Z0-9.]*)}}}");
                
                Match m = r.Match(value);
                while(m.Success)
                {
                    String tmp = m.Value;
                    string val = getValue(ObjectType.VALUE, tmp.Replace("{", "").Replace("}", "")).ToString();
                    value=value.Replace(tmp, val);
                    m=r.Match(value);
                }
            }

            return value;
        }

        public static int stringToInt(string s)
        {
            return Int32.Parse(s);
        }

        public static double stringToDouble(string s)
        {
            return double.Parse(s);
        }
    }
    
        
}
