﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GdzieJaIde.Datamodel.Enums;
using System.Threading;
using Microsoft.Phone.Controls;

namespace GdzieJaIde.API
{
    public class GUI
    {
        public const string MESSAGE = "message";
        public const string WARNING = "warning";
        public const string CHOICE = "choice";
        public const string MULTIPLE_CHOICE = "multipleChoice";
        public const string DIALOG = "dialog";
        public const string CONFIRM = "confirm";
        public const string SHOW_MEDIA = "showMedia";
        public static int id = -1;

        public static void message(String title, String message)
	    {
            Dialogs dialog = new Dialogs();
            //dialog.ShowMessage(title, message);
            dialog.ShowMessage(title, message, ++id);
            //(((App)Application.Current).RootFrame.Content as PhoneApplicationPage).NavigationService.Navigate(new Uri("/ObjectListPage.xaml?Type=Item&CurrZone=1", UriKind.Relative));
	    }

        public static void warning(String title, String message)
	    {

	    }

        public static int choice(String[] elements)
	    {
		    return 0;
	    }

        /*public static int[] multipleChoice(String[] elements)
	    {
		    return new int[0];
	    }
        */
        public static string dialog(string title, string message)
	    {
            Dialogs dial = new Dialogs();
            //dial.ShowDialog(title, message, ++id);
            return dial.ShowDialog(title, message, ++id);
            //(((App)Application.Current).RootFrame.Content as PhoneApplicationPage).NavigationService.Navigate(new Uri("/ObjectListPage.xaml?Type=Character&CurrZone=1", UriKind.Relative));
            //return "";
	    }

        public static bool confirm(String title, String message)
	    {
		    return false;
	    }

        public static bool showMedia(int mediaID)
	    {
		    return false;
	    }
    }
}
