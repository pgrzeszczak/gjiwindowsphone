﻿using System;

namespace GdzieJaIde.API
{
    public class MATH
    {
        public const string  ABS = "abs";
        public const string  ADD="add";
        public const string  DIV="div";
        public const string  MAX="max";
        public const string  MIN="min";
        public const string  MINUS="minus";
        public const string  MOD="mod";
        public const string  MULTI="multi";
        public const string  POWER="power";
         public const string  ROOT="root";
         public const string  ROUND="round";
        public const string  SUM="sum";

        public static int sum(int[] args)
        {
            int result = 0;
            foreach (int i in args)
            {
                result += i;
            }
            return result;
        }

        public static double sum(double[] args)
        {
            double result = 0;
            foreach (double i in args)
            {
                result += i;
            }
            return result;
        }

        public static int add(int a, int b)
        {
            return a + b;
        }

        public static double add(double a, double b)
        {
            return a + b;
        }

        public static int minus(int a, int b)
        {
            return a - b;
        }

        public static double minus(double a, double b)
        {
            return a - b;
        }

        public static int abs(int a)
        {
            return Math.Abs(a);
        }

        public static double abs(double a)
        {
            return Math.Abs(a);
        }

        public static int div(int a, int b)
        {
            return a / b;
        }

        public static double div(double a, double b)
        {
            return a / b;
        }

        public static int mod(int a, int b)
        {
            return a%b;
        }

        public static int multi(int a, int b)
        {
            return a*b;
        }

        public static double multi(double a, double b)
        {
            return a*b;
        }

        public static int power(int a, int b)
        {
            return (int)Math.Pow(a, b);
        }

        public static double power(double a, double b)
        {
            return (double) Math.Pow(a, b);
        }

        public static double root(double a, double b)
        {
            return (double) Math.Pow(a, 1/b);
        }

        public static int min(int a, int b)
        {
            return Math.Min(a, b);
        }

        public static double min(double a, double b)
        {
            return Math.Min(a, b);
        }

        public static int max(int a, int b)
        {
            return Math.Max(a, b);
        }

        public static double max(double a, double b)
        {
            return Math.Max(a, b);
        }

        public static int round(double a)
        {
            return (int) Math.Round(a);
        }
    }
}
